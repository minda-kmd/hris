<?php 
	Class Globalproc extends CI_Model {
		
		
		public function __construct() {
			parent::__construct();
			$session_database = $this->session->userdata('database_default');
			$this->load->database($session_database, TRUE);
		}
		
		public function __save($table, $values) {
			// $a = $this->load->database();
			$session_database = $this->session->userdata('database_default');
			$this->load->database($session_database, TRUE);
			
			$sql = null;
			if (is_array($values)) {
				$sql = "insert into {$table} (";
				$count = 0;
				foreach (array_keys($values) as $ks) {
					$sql .= $ks;
					$sql .= ($count == count($values)-1) ? "" : ", ";
					$count++;
				}
				$sql .= ") values (";

				$count = 0;
				foreach($values as $key => $vals) {
					//$sql .= "'".$vals."'";
					$sql .= $this->db->escape($vals);
					$sql .= ($count == count($values)-1)? "": ",";
					$count++;
				}
				$sql .= ")";
			}
		
			$ret = $this->db->query($sql);
			$this->db->close();
			return $ret;
		}

		public function run_sql($sql) {
			$session_database = $this->session->userdata('database_default');
			$this->load->database($session_database, TRUE);
			
			$ret = $this->db->query($sql);
			$this->db->close();
			return $ret;
		}
		
		public function __update($table, $values, $where) {
			// $this->load->database();
			$session_database = $this->session->userdata('database_default');
			$this->load->database($session_database, TRUE);
			
			$sql = null;

			if (is_array($values)) {
				$count = 0;
				$sql = "update {$table} set ";

				foreach($values as $key => $vals) {
					$sql .= $key."='".$vals."'";
					$sql .= ($count==count($values)-1)?"":", ";
					$count++;
				}

				$count = 0;
				// conn = and 
				// conn = or
				if (is_array($where)){
					$sql .= " where ";
					foreach($where as $key => $val) {
						if ($key == "conn") {
							$sql .= " ".$val." ";
						} else {
							$sql .= $key."='".$val."'";	
						}
					}
				}
				
				$ret = $this->db->query($sql);
				$this->db->close();
				return $ret;
			}

			return false;

		}

		public function __getdata($sql) { //, $table = false
			// $this->load->database();
	
			$ret = null;

			if (!is_array($sql)) {
				$ret = $this->db->query($sql);
			} else {
				// write a query for values sent in array
				// for the meantime run code in sql form
			}
			$this->db->close();
			return $ret->result();
		}

		public function getrecentsavedrecord($table, $callback) {
			$this->load->database();
			$sql = "SELECT IDENT_CURRENT('".$table."') as ".$callback;  
			$ret = $this->db->query($sql)->result();
			$this->db->close();
			return $ret;
		}


		public function __checkindb($table, $field, $value) {
			$this->load->database();

			$sql = "select * from {$table} where {$field}='{$value}'";
			$ret = $this->db->query($sql);

			$this->db->close();
			
			if (count($ret)-1 >= 1) {
				return true;
			} else {
				return false;
			}
		}

		public function __createuniqueid($word) {
			// md5 userid
			// date and time today
			return (substr(md5($word),0,6)."-".substr(md5(date("ldY:hisa")),0,6));
		}

		public function __datetoday() {
			return date("l, F d, Y");
		}
		
		public function thedate() {
			return date("F d, Y");
		}
		
		public function monthcover() {
			return date("M Y");
		}

		public function merge_new_values($default, $new) {
			foreach($new as $key => $vals) {
				$default[$key] = $vals;
			}

			return $default;
		}

		public function get_empdetails($details) {
			$this->load->model("Globalvars");

			$loggedid = $this->Globalvars->employeeid;

			$sql = "select * from employees where employee_id = '{$loggedid}'";
			$var = $this->__getdata($sql);

			$ret_vars = [];

			foreach($details as $det_key) {
				$ret_vars[$det_key] = $var[0]->$det_key;
			}

			return $ret_vars;
		}

		public function get_details_from_table($table, $where, $details) {
/*
			$this->load->model("Globalvars");
			$loggedid = $this->Globalvars->employeeid;
*/
			$sql = "select * from {$table} "; // "where employee_id = '{$loggedid}'";

			if (is_array($where)) {
				$sql .= " where ";
				
				foreach($where as $key => $val) {
					if ($key == "conn") {
						$sql .= " ".$val." ";
					} else {
						$sql .= $key."='".$val."'";
					}
				}

			} else {
				$sql .= " ".$where." ";
			}

			$var = $this->__getdata($sql);

			$ret_vars = [];

			foreach($details as $det_key) {
				$ret_vars[$det_key] = $var[0]->$det_key;
			}

			return $ret_vars;
			
		}
		
		public function gdtf($table, $where, $details) {
			$sql = "SELECT ";
			
			if (is_array($details)){
				$count = 0;
				foreach($details as $d) {
					$sql .= $d;
					$sql .= (count($details)-1==$count)?"":",";
					$count++;
				}
			} else {
				$sql .= $details;
			}
			
			$sql .= " FROM ".$table;
			$sql .= " WHERE ";

			if (is_array($where)){
				$count = 0;
				foreach($where as $key => $val) {
					if ($key == "conn") {
						$sql .= " ".$val." ";
					} else {
						$sql .= $key."='".$val."'";
					}
				}
			} else {
				$sql .= " ".$where." ";
			}	
			
			$var = $this->__getdata($sql);
			
			return $var;
		}

		public function get_leave_dbid($leavecode) {
			$sql = "select * from leaves where leave_code = '{$leavecode}'";

			$a   = $this->__getdata($sql);

			return $a[0]->leave_id;

		}

		public function compute_time_interval($from,$to) {
			// from 3:00 PM
			// to 6:00 PM
			$hrs  = "";
			$mins = "";
			$ampm = ""; // can be am or pm or both

			$interval = strtotime($from)-strtotime($to);
			return $interval/60/60;
		}
		
		public function sendtoemail($details){
			/*
			$to 	 = $details['to'];
			$subject = $details['subject'];
			$message = $details['message'];
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= "From: ".$details["from"]. "\r\n";
			
			if (isset($details['cc'])) {
				$headers .= "Cc: ".$details["cc"]. "\r\n";
			}
			
			return mail($to, $subject, $message, $headers);
			*/
			
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => 'minda.smtpsender@gmail.com',
				'smtp_pass' => 'ghty56rueiwoqp',
				'mailtype'  => 'html', 
				'charset'   => 'iso-8859-1'
			);
			
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			
			$this->email->from('noreply@minda.gov.ph', $details["from"]);
			$this->email->to($details['to']);
			$this->email->subject($details['subject']);
			
			if (isset($details['cc'])){
				$this->email->cc($details['cc']);
			}
			
			$this->email->message($details['message']);
			
			return $this->email->send();
		}
		
		public function tokenizer_leave($exactid) {
			return substr(md5(substr(md5($exactid),0,11)),0,11);
		}
		
		public function check_leavecredits($empid,$return = false) {
			// check for no entry
			$elc = $this->gdtf("employee_leave_credits",["employee_id"=>$empid],["elc_id","vl_value","sl_value"]);
			
			if (count($elc)==0) {
				return;
			} else {
				$elc_latest = $this->gdtf("employee_leave_credits",
										  "elc_id = (select max(elc_id) from employee_leave_credits where employee_id = '{$empid}')",
									      "*");
				if ($return==true) {
					return $elc_latest;
				} else {
					return true;
				}
			}
			
			return false;
		}
		
		public function convert($value, $type) {
			
			$conversion = $this->gdtf("hours_minutes_conversation_fractions",["particular"=>$value,"conn"=>"and","type"=>$type],["equi_day"]);
			
			return $conversion[0]->equi_day;
		}
		
		public function calc_leavecredits($empid, $exactid, $type_details) {
			
		$flag = false;		
		/*
		type_details
			typemode
			leave_value 
			value 								// not in use
			date_inclusion
			no_days_applied
			hrs
			mins
		end type details
		*/

		//	$empid = 50;
			// ============================================================================================================================
			// DEDUCTIONS	
				// leave credits
				if ( strtoupper($type_details['typemode']) != "CTO") {
					$leavecredits = $this->gdtf("employee_leave_credits",
													"elc_id = (select max(elc_id) from employee_leave_credits where employee_id = '{$empid}')",
													"*");
					
					if (count($leavecredits)==0) {
						// return NULL if nothing is found in the database
						$leavecredits[] = (object) ["vl_value"	 => 0,
													"fl_value"	 => 0,
													"sl_value"	 => 0,
													"coc_value"	 => 0,
													"spl_value"  => 0
												    ];
					
					} 
					$details = [
							"employee_id" 		=> $empid,
							"vl_value" 			=> $leavecredits[0]->vl_value,
							"fl_value"			=> $leavecredits[0]->fl_value,
							"sl_value"			=> $leavecredits[0]->sl_value,
							"spl_value"			=> $leavecredits[0]->spl_value,
							"coc_value"			=> $leavecredits[0]->coc_value,
							"credits_as_of"		=> $this->thedate(),
							"is_beggining"		=> 1,
							"is_current"		=> 1,
							"exact_id"			=> $exactid,
							"vl_earned"			=> 0,
							"sl_earned"			=> 0,
							"elc_type"			=> $type_details['typemode'],
							"hrs"				=> 0,
							"mins"				=> 0,
							"withpay"			=> 0,
							"wopay"				=> 0
						];
				}
				// conversion 
				
				// LEAVE ======================================================================================
					if ( strtoupper($type_details['typemode']) == "LEAVE") {
						// sick leave
						if ($type_details['leave_value'] == 1) {
							$subtrahend = $type_details['no_days_applied'];
							
							if (isset($type_details['hrs']) && !empty($type_details['hrs'])) {
								$subtrahend 		= $this->convert($type_details['hrs'], "h");
								$details['hrs']		= $type_details['hrs'];
							}
							
							if (isset($type_details['mins']) && !empty($type_details['mins'])) {
								$subtrahend 		+= $this->convert($type_details['mins'], "m");
								$details['mins']	= $type_details['mins'];
							}
						
							if (isset($type_details['value']) && $type_details['value'] == "declined") {
								$details["withpay"] = 0;
								$details["wopay"]   = $subtrahend;
							} else {
								// for days	
								if ($leavecredits[0]->sl_value < $subtrahend){
									if ($leavecredits[0]->sl_value <= 0) {
										$details["withpay"] = 0;
										$details["wopay"]   = $subtrahend;
									} else {
										$details["withpay"] = $leavecredits[0]->sl_value;
										$details["wopay"]   = $subtrahend - $leavecredits[0]->sl_value;
									}
									$details['sl_value'] = 0;
								} else if ($leavecredits[0]->sl_value == $subtrahend){
									$details["withpay"] = $leavecredits[0]->sl_value;
									$details["wopay"]   = 0;
									
									$details['sl_value'] = $leavecredits[0]->sl_value - $subtrahend;
								} else if ($leavecredits[0]->sl_value > $subtrahend){
									$details["withpay"] = $subtrahend;
									$details["wopay"]   = 0;
									
									$details['sl_value'] = $leavecredits[0]->sl_value - $subtrahend;
								} 
								// end for days

								/*
								$prev_sl 			 = $leavecredits[0]->sl_value;
								$details['sl_value'] = $prev_sl - $type_details['no_days_applied'];
								*/
							}
						}
						
						// vacation leave
						else if ( $type_details['leave_value'] == 2) {
							if (isset($type_details['value']) && $type_details['value'] == "declined") {
								$details["withpay"] = 0;
								$details["wopay"]   = $type_details['no_days_applied'];
							} else {
								if ($leavecredits[0]->vl_value < $type_details['no_days_applied']){
									if ($leavecredits[0]->vl_value <= 0) {
										$details["withpay"] = 0;
										$details["wopay"]   = $type_details['no_days_applied'];
									} else {
										$details["withpay"] = $leavecredits[0]->vl_value;
										$details["wopay"]   = $type_details['no_days_applied'] - $leavecredits[0]->vl_value;
									}
									$details['vl_value'] = 0;
								} else if ($leavecredits[0]->vl_value == $type_details['no_days_applied']){
									$details["withpay"] = $leavecredits[0]->vl_value;
									$details["wopay"]   = 0;
									
									$details['vl_value'] = $leavecredits[0]->vl_value - $type_details['no_days_applied'];
								} else if ($leavecredits[0]->vl_value > $type_details['no_days_applied']){
									$details["withpay"] = $type_details['no_days_applied'];
									$details["wopay"]   = 0;
									
									$details['vl_value'] = $leavecredits[0]->vl_value - $type_details['no_days_applied'];
								}
							}
						}
						
					// SPL ============================================================================================
						else if ( strtoupper($type_details['typemode']) == "SPL" || $type_details['leave_value'] == 4) {
							// data for SPL here
							
							$details['spl_value']	  = $leavecredits[0]->spl_value - $type_details['no_days_applied']; // 1
							//$details['credits_as_of'] = $type_details['date_inclusion'];
						}
					// SPL ============================================================================================
					
					// FL :: forced leave =============================================================================
						else if ( strtoupper($type_details['typemode']) == "FL" || $type_details['typemode'] == "fl" || $type_details['leave_value'] == 6) {
							// data for FL here
							
							$details['fl_value']	  = $leavecredits[0]->fl_value - $type_details['no_days_applied'];
							//$details['credits_as_of'] = $type_details['date_inclusion'];
						} else {
							if (isset($type_details['value']) && $type_details['value'] == "declined") {
								$details["withpay"] = 0;
								$details["wopay"]   = $type_details['no_days_applied'];
							}
						}
					// end FL ============================================================================================
						$details['credits_as_of'] = $type_details['date_inclusion'];
						
					}
				// END LEAVE ======================================================================================
				
				
				
				// PS =============================================================================================
					if (strtoupper($type_details['typemode']) == "PS" 
						|| strtoupper($type_details['typemode']) == "T" 
							|| strtoupper($type_details['typemode']) == "UT") {
						// leave_value
						$details['hrs']			  = $type_details['hrs'];
						$details['mins']		  = $type_details['mins'];
						$details['credits_as_of'] = $type_details['date_inclusion'];
						
						$converted_hrs  = ($type_details['hrs'] == 0 || $type_details['hrs'] == null)?0:$this->convert($type_details['hrs'],"h");
						$converted_mins = ($type_details['mins'] == 0 || $type_details['mins'] == null)?0:$this->convert($type_details['mins'],"m");
							
						$total_hrs_mins = $converted_hrs + $converted_mins;
						
						$details["withpay"] = 0;
						$details["wopay"]   = 0;
						
						if ($type_details['leave_value'] == 1) { // official
							// record the PS but no deductions
							$details['wopay'] 	 = 0;
							$details['withpay']	 = $total_hrs_mins;
						} else if ($type_details['leave_value'] == 2 
							|| strtoupper($type_details['typemode']) == "T"
								|| strtoupper($type_details['typemode']) == "UT") { // personal, tardiness and undertime
							if ($leavecredits[0]->vl_value < $total_hrs_mins) {
								if ($leavecredits[0]->vl_value <= 0) {
									$details['wopay'] 	 = $total_hrs_mins;
									$details['withpay']	 = 0;
									$details['vl_value'] = 0;
								} else {
									$details['wopay'] 	 = $total_hrs_mins - $leavecredits[0]->vl_value;
									$details['withpay']	 = $leavecredits[0]->vl_value;
									$details['vl_value'] = 0;
								}
							} else if ($leavecredits[0]->vl_value == $total_hrs_mins) {
								$details['vl_value'] = 0;
								$details['wopay'] 	 = $total_hrs_mins;
								$details['withpay']	 = 0;
							} else if ($leavecredits[0]->vl_value > $total_hrs_mins) {
								$details['vl_value'] = $leavecredits[0]->vl_value - $total_hrs_mins;
								$details['wopay'] 	 = 0;
								$details['withpay']	 = $total_hrs_mins;
							}
							
						}
					}
				// end PS ==========================================================================================
							
				// OB ===================================================================================================
					$details['credits_as_of'] = $type_details['date_inclusion'];
				// end OB ===============================================================================================
				
				// PAF ========================================================================
					if ( strtoupper($type_details['typemode']) == "PAF" ) {
						$details['hrs']		= $type_details['hrs'];
					}
				// end PAF ====================================================================
				
				// mark cto global
				// cto =========================================================================
					if ($type_details['typemode'] == "CTO" || $type_details['typemode'] == "cto") {
						// adding of OT to OT database :: employee_ot_credits						
						
						// ** important
							// add CTO deductions
						// end 
						
					//	$start_time  = new DateTime('10:00 AM');
					//	$end_time    = new DateTime('3:00 PM');
						
						$start_time  = new DateTime($type_details["hrs_start"]);
						$end_time    = new DateTime($type_details["hrs_end"]);
						$interval    = $start_time->diff($end_time);
						$cto_hours 	 = $interval->format('%h');
						$cto_mins    = $interval->format('%i');
						$total_hours = $cto_hours.".".$cto_mins;
						
						$sql = "select * from employee_ot_credits 
								where elc_otcto_id = (select max(elc_otcto_id) 
													  from employee_ot_credits as et 
													  where emp_id = '{$type_details['empid']}')";
						
						$ddata 		 = $this->Globalproc->__getdata($sql);
						$totalcredit = $ddata[0]->total_credit;
						
						$grandtotal  = $totalcredit - $total_hours;
						
						$ot_details = [
							"date_of_application"	=> $type_details['date_inclusion'],
							"total"					=> $total_hours,
							"cred_total"			=> $totalcredit,
							"total_credit"			=> $grandtotal,
							"emp_id"				=> $type_details['empid'],
							"cto_hours"				=> $cto_hours,
							"cto_mins"				=> $cto_mins,
							"cto_start"				=> $type_details['hrs_start'],
							"cto_end"				=> $type_details['hrs_end'],
							"credit_type"			=> "CTO",
							"exact_ot"				=> $type_details['exact_ot']
						];
						
						$flag = $this->__save("employee_ot_credits",$ot_details);
						
						if ($flag) {
							return true;
						}
						return false;
					}
				// end of cto ==================================================================
				
			// END DEDUCTIONS
			// =====================================================================================================
			$flag = $this->__save("employee_leave_credits",$details);
			return $flag;
		}
		
		function saveto_ot($values) {
			
			/*
			$values = [
				"am_in" 			=> "10:00 AM",
				"am_out"			=> "12:00 PM",
				"calc_elc"			=> true,
				"dates"				=> ["2/18/2018"],
				"dbm_chief_id"		=> 0,
				"division_chief_id"	=> 0,
				"empid"				=> 62,
				"isam"				=> true,
				"ispm"				=> true,
				"mult"				=> 1.5,
				"pm_in"				=> "1:00 PM",
				"pm_out"			=> "5:00 PM",
				"remarks_ot"		=> "",
				"tasktobedone"		=> "",
				"tasktype"			=> null,
				"timein"			=> "",
				"timeout"			=> "",
				"typemode"			=> "OT"
			];
			*/
			
			$type_details = [
				"date_of_application" 	=> date("m/d/Y", strtotime($values['dates'][0])),
				"am_in"					=> null,
				"am_out"				=> null,
				"pm_in"					=> null,
				"pm_out"				=> null,
				"total"					=> null,
				"cred_total"			=> null,
				"mult"					=> $values['mult'],
				"total_credit"			=> null,
				"emp_id"				=> $values['empid'],
				"credit_type"			=> "OT",
				"exact_ot"				=> $values['exact_ot']
			];
	
			$am_total_hours = "00:00";
			$pm_total_hours = "00:00";
				
			if (isset($values['isam'])) {
				$type_details["am_in"]  = $am_in  = $values['am_in'];
				$type_details["am_out"] = $am_out = $values['am_out'];
					
				$datetime1 = new DateTime($am_in);
				$datetime2 = new DateTime($am_out);
				$interval = $datetime1->diff($datetime2);
					
				$am_total_hours = $interval->format("%h").":".$interval->format("%i");
			
			}
				
			if (isset($values['ispm'])) {
				$type_details["pm_in"]  = $pm_in  = $values['pm_in'];
				$type_details["pm_out"] = $pm_out = $values['pm_out'];
					
				$datetime1 = new DateTime($pm_in);
				$datetime2 = new DateTime($pm_out);
				$interval = $datetime1->diff($datetime2);
					
				$pm_total_hours = $interval->format("%h").":".$interval->format("%i");
			}
				
			$secs        = strtotime($pm_total_hours)-strtotime("00:00:00");
			$result      = date("H:i",strtotime($am_total_hours)+$secs);
				
			$result_hour = date("H",strtotime($am_total_hours)+$secs);
			$result_mins = date("i",strtotime($am_total_hours)+$secs);
							
			// not converted hour into days :: unit used is in hour
				$total_hours_in_days 		  = $result_hour.".".$result_mins;
				$type_details['total'] 	      = $result;			// no_days_applied
				
				$type_details['cred_total']   = $total_hours_in_days * $type_details['mult'];
				
				$sql = "select * from employee_ot_credits 
						where elc_otcto_id = (select max(elc_otcto_id) 
											  from employee_ot_credits as et 
											  where emp_id = '{$values['empid']}')";
				
				$rem = $this->__getdata($sql);
				
				$type_details['total_credit'] = $type_details['cred_total'];
				
				if (count($rem) != 0){
					$type_details['total_credit'] = $rem[0]->total_credit + $type_details['cred_total'];
				}
				
			// end 
			
			$save = $this->__save("employee_ot_credits",$type_details);
			
			return $save;
			
		}
		
		function getleaves() {
		//	$this->load->model("v2main/Globalproc");
			
			$ret = $this->__getdata("SELECT * from leaves");
			
			$data = [];
			for ($i = 0; $i <= count($ret)-1 ; $i++) {
				$data[$ret[$i]->leave_id] = $ret[$i]->leave_name;
			}
			
			return $data;
			
			//echo json_encode($ret[0]->leave_name);
			//echo json_encode($ret[0]->leave_id);
			
		}
		
		function get_spl_count($empid) {
			/*
			$sql = "select * from checkexact as ce
					JOIN checkexact_leave_logs as cll on ce.exact_id = cll.exact_id
					where ce.employee_id ='{$empid}' and ce.leave_id = 4 and is_approved = 1"; // 
			*/
			
			// $empid 	   = 62;
			$this_year = date("Y");
			/*
			$sql = "select * from checkexact_leave_logs as cll 
					JOIN checkexact as ce ON cll.grp_id = ce.grp_id 
					JOIN employees as e on ce.employee_id = e.employee_id
					where e.employee_id = '{$empid}' and ce.leave_id = 4 and ce.is_approved = 1";
			*/		
			$sql = "select 
						distinct(tb1.grp_id),
						YEAR (tb1.checkdate) as year
					from 
						(select 
							distinct(cll.grp_id),
							ce.checkdate
						from checkexact_leave_logs as cll 
						JOIN checkexact as ce ON cll.grp_id = ce.grp_id 
						JOIN employees as e on ce.employee_id = e.employee_id
						where e.employee_id = '{$empid}' 
						and ce.leave_id = 4 
						and ce.is_approved = 1)
						as tb1
					where YEAR( tb1.checkdate ) = '{$this_year}'";
				
			$spl = $this->__getdata($sql);
			
			/*
			$spl_count = 0;
			if ( count($spl) != 0 ) {
				$grp_id = 0;
				$this_year = date("Y");
				foreach($spl as $s) {
					$grp_id = $s->grp_id;;
					$year_app = date("Y", strtotime($s->checkdate));
					if ($year_app == $this_year && $s->grp_id != $grp_id) {
						$spl_count++;
					}
				}
			}
			
			return $spl_count;
			*/
			
			return count($spl);
		}
		
		function getvl_count($empid) {
			$sql = "select * from employee_leave_credits 
					where elc_id = (SELECT max(elc_id) from employee_leave_credits where employee_id = '{$empid}')";
			
			return $this->__getdata($sql);
		}
		
		function check_prev_dtr($empid) {
			return $this->gdtf("countersign",['emp_id'=>$empid,"conn"=>"and","hrnotified"=>0],["approval_status","hrnotified","countersign_id"]);
		}
		
		function checkforactive_coverage($emp_type) {
			return $this->gdtf("hr_dtr_coverage",['employment_type'=>$emp_type, "conn"=>"and", "is_active" => true],"*");
		}
		
		function get_signatories($empid) {
			$this->load->model('main/main_model');
			
			$emp_dets 		= $this->gdtf("employees",["employee_id"=>$empid],["Division_id","DBM_Pap_id"]);
			$emp_div_id 	= $emp_dets[0]->Division_id;
			$emp_dbm_id     = $emp_dets[0]->DBM_Pap_id;
		
			// division
				$division 			    	= $this->main_model->array_utf8_encode_recursive( $this->gdtf("employees",["Division_id"=>$emp_div_id,"conn"=>"and","is_head"=>1],["email_2","f_name","employee_id"]) );
				
				if ( count($division) == 0) {
					return;
				}
				
				$division_chief_email 		= $division[0]->email_2; 
				$division_chief_fname 	 	= $division[0]->f_name;
				$division_chief_id 			= $division[0]->employee_id;
			// end division
				
			// DBM 
				$the_dbm = $emp_dbm_id;
				
				/*
				if ($emp_dbm_id == 5) {
					$the_dbm = 3;
				}
				*/
				
				$sql			= "select email_2, employee_id, f_name 
								   from employees 
								   where DBM_Pap_id='{$the_dbm}' and 
								   Division_id = 0 and is_head = 1";
				$dbm 			= $this->main_model->array_utf8_encode_recursive( $this->__getdata($sql) );
				
				if (count($dbm)==0) { return; }
				
				$dbm_email 		= $dbm[0]->email_2;
				$dbm_fname 		= $dbm[0]->f_name;
				$dbm_id 		= $dbm[0]->employee_id;
			// end DBM
			
			
			// =========
				$division_sign  = [
					"div_name" 	=> $division_chief_fname,
					"div_email" => $division_chief_email,
					"div_empid" => $division_chief_id
					];
				$dbm_sign 	    = [
					"dbm_name"	=> $dbm_fname,
					"dbm_email"	=> $dbm_email,
					"dbm_empid"	=> $dbm_id
					];
				
				$div_other_sign = [];
				$dbm_other_sign = [];
			// =========
			
			// get the division emps 
				$div_sql = $this->main_model->array_utf8_encode_recursive( $this->gdtf("employees",["Division_id"=>$emp_div_id,"conn"=>"and","status"=>1],["email_2","f_name","employee_id"]) );
				
					$count = 0;
					foreach($div_sql as $divs) {
						$div_other_sign[$count]["fname"]  = $divs->f_name;
						$div_other_sign[$count]["email"]  = $divs->email_2;
						$div_other_sign[$count]["emp_id"] = $divs->employee_id;
						$count++;
					}
			// end division emps
			
			// get the DBM signatories
				$dbm_sql = $this->main_model->array_utf8_encode_recursive( $this->gdtf("employees",["DBM_Pap_id"=>$emp_dbm_id,"conn"=>"and","status"=>1],["email_2","f_name","employee_id"]) );
					
					$count_1 = 0;
					foreach($dbm_sql  as $ds) {
						$dbm_other_sign[$count_1]["fname"]  = $ds->f_name;
						$dbm_other_sign[$count_1]["email"]  = $ds->email_2;
						$dbm_other_sign[$count_1]["emp_id"] = $ds->employee_id;
						$count_1++;
					}
			// end DBM signatories
		
			return Array("division"		  => $division_sign,
						 "dbm" 	   		  => $dbm_sign,
						 "division_other" => $div_other_sign,
						 "dbm_other"	  => $dbm_other_sign);
			
		}
		
		public function get_signatories_forchiefs($level) {
			$this->load->model("Globalvars");
			$emp = $this->Globalvars->employeeid;
			if ($level == "division") {
				// get signatories for division chiefs
				$emp_des = $this->Globalproc->gdtf("employees",["employee_id"=>$emp],["Division_id","DBM_Pap_id"]);
				$sql 	 = "select * from employees where DBM_Pap_id = '{$emp_des[0]->DBM_Pap_id}' and Division_id = '0' and is_head = '1'";
				$app_	 = $this->Globalproc->__getdata($sql);
				var_dump($app_);
			} else if ($level == "director") {
				// get signatories for director level
			}
			
		}
		
		public function is_chief($level, $emp_id) {
			$sql = "select employee_id from employees where employee_id = '{$emp_id}'";
		
			switch($level) {
				case "division":
					$sql .= " and is_head = 1 and Division_id <> 0";
					break;
				case "director":
					$sql .= " and is_head = 1 and Division_id = 0";
					break;
			}
			$ret = $this->__getdata($sql);
			
			if (count($ret) == 0){
				return false;
			}
			return true;
		}
		
		public function allow_auto_login($details) {
			/*
			$username = $details['username'];
			$password = $details['password'];
			
			$info["username"] = $this->uri->segment(5);
			$info['password'] = $this->uri->segment(4);
			*/
			
			$a = $this->Login_model->authorizeUser($details);
		
			$emp_id   = $a[0]->employee_id;
			$username = $a[0]->username;
			$usertype = $a[0]->usertype;
			
			$result2 = $this->Login_model->getUserInformation($a[0]->employee_id);

			$user_session = array(
				'employee_id' => $emp_id,
				'username' => $username,
				'usertype' => $usertype,

				//'first_name' => $result2[0]->first_name,
				'full_name' => $result2[0]->f_name,
				'first_name' => $result2[0]->firstname,
				'last_name' => $result2[0]->l_name,
				'biometric_id' => $result2[0]->biometric_id,
				//'user_email' => $result2[0]->email,
				//'user_position' => $result2[0]->position_name,
				'area_id' => $result2[0]->area_id,
				'area_name' => $result2[0]->area_name,
			  
				//'display_name' => $result2[0]->display_name,
				'ip_address' => $_SERVER["REMOTE_ADDR"],
				//'theme_color' => $result2[0]->theme_color,
				//'bg_image' => $result2[0]->bg_image,
				//'font_color' => $result2[0]->font_color,
				//'ticket_log_order_start_on' => $result2[0]->ticket_log_order_start_on,
				'is_logged_in' => TRUE,
				'database_default' => 'sqlserver',
				'employment_type' => $result2[0]->employment_type,
				'employee_image' => $result2[0]->employee_image,
				'level_sub_pap_div' => $result2[0]->Level_sub_pap_div,
				'division_id' => $result2[0]->Division_id,
				'dbm_sub_pap_id' => $result2[0]->DBM_Pap_id,
				'is_head' => $result2[0]->is_head,
				'office_division_name' => $result2[0]->office_division_name,
				'position_name' => $result2[0]->position_name

				//'profile_picture' => $result2[0]->image_path
			);
			
			return $this->session->set_userdata($user_session);
		}
		
		public function return_remaining( $remainingwhat, $empid ) {
			$rem_ 		= null;
			$fld_value  = null;
			
			switch($remainingwhat) {
				case "VL":
					$rem_ = "vl_value";
					$fld_value = 2;
					break;
				case "SL":
					$rem_ = "sl_value";
					$fld_value = 1;
					break;
				case "SPL":
					$rem_ = "spl_value";
					$fld_value = 4;
					break;
				case "FL":
					$rem_ = "fl_value";
					$fld_value = 6;
					break;
			}
			
			$sql = "select {$rem_} 
					from employee_leave_credits
					where 
					elc_id = (select max(elc_id) 
							  from employee_leave_credits as elc
							  where elc.employee_id = '{$empid}')";
			
					/*
					// join the checkexact and check what kind of leave it is.
					*/
					
					
		//	echo $sql;
			$ret = $this->__getdata($sql);
			
			return (count($ret)<=0)?0:$ret[0]->$rem_;
		}
		
		public function allow_no_password($user_id) {
			
			$result2      = $this->Login_model->getUserInformation($user_id);
			
			$user_session = array(
				'employee_id' => $emp_id,
				'username' => $username,
				'usertype' => $usertype,

				//'first_name' => $result2[0]->first_name,
				'full_name' => $result2[0]->f_name,
				'first_name' => $result2[0]->firstname,
				'last_name' => $result2[0]->l_name,
				'biometric_id' => $result2[0]->biometric_id,
				//'user_email' => $result2[0]->email,
				//'user_position' => $result2[0]->position_name,
				'area_id' => $result2[0]->area_id,
				'area_name' => $result2[0]->area_name,
			  
				//'display_name' => $result2[0]->display_name,
				'ip_address' => $_SERVER["REMOTE_ADDR"],
				//'theme_color' => $result2[0]->theme_color,
				//'bg_image' => $result2[0]->bg_image,
				//'font_color' => $result2[0]->font_color,
				//'ticket_log_order_start_on' => $result2[0]->ticket_log_order_start_on,
				'is_logged_in' => TRUE,
				'database_default' => 'sqlserver',
				'employment_type' => $result2[0]->employment_type,
				'employee_image' => $result2[0]->employee_image,
				'level_sub_pap_div' => $result2[0]->Level_sub_pap_div,
				'division_id' => $result2[0]->Division_id,
				'dbm_sub_pap_id' => $result2[0]->DBM_Pap_id,
				'is_head' => $result2[0]->is_head,
				'office_division_name' => $result2[0]->office_division_name,
				'position_name' => $result2[0]->position_name

				//'profile_picture' => $result2[0]->image_path
			);
			
			return $this->session->set_userdata($user_session);
			
		}
		
		public function returndtrformat($f_name,$coverage,$vercode,$uname,$pword,$bodycode, $accom_view, $approvedby) {
		/*
			$m = "<div style='width:100%; background:#ededed; padding: 18px 0px; font-size: 15px;'>
										<div style='width: 85%; margin: auto; border: 1px solid #9c9c9c; background: #fff; border-radius: 2px; font-family: arial; box-shadow: 0px 0px 4px #9e9e9e;'>
											<table style='width:100%;'>
												<tr>
													<td style='width:30%; vertical-align: top;'>
														<table style='width:100%;background: #ddd; border-collapse: collapse;'>
															<tr>
																<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
																	From:
																</td>
																<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc;'>
																	{$f_name} 
																</td>
															</tr>	
															<tr>
																<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
																	DTR Coverage:
																</td>
																<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc;'>
																	{$coverage}
																</td>
															</tr>
															<tr>
																<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
																	Approved By:
																</td>
																<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc;'>
																	not yet approved
																</td>
															</tr>
															<tr style=''>
																<td colspan=2 style='text-align: center; padding: 10px 5px;'>
																	<a href='".base_url()."dtr/approval/".$vercode."/".$pword."/".$uname."' style='text-decoration:none;'>
																		<p style='padding: 12px 15px; 
																				  padding: 7px 15px;
																				  text-decoration:none;
																				  background: #5ce6e1;
																				  border: 1px solid #50a4ae;
																				  font-size: 16px;
																				  margin: 0px;'> Approve 
																		<p>
																	</a>
																</td>
															</tr>
															<tr style='border-top: 1px solid #fcfcfc;'>
															<td colspan='2' style='text-align: center; padding: 10px 5px;'>
																<a href='".base_url()."/dtr/forapproval/' style='text-decoration:none;'>
																   <p style='padding: 12px 15px;
																		   margin:0px;
																		  background: #fff;						
																		  border: 1px solid #b9b9b9;
																		  cursor: pointer;
																		  font-size: 16px;
																		  box-shadow: 0px 2px 0px #cbcbcb;'> View all DTR 
																	</p>
																</a>
															</td>
														</tr>
														</table>
													</td>
													<td rowspan=6 style='padding: 10px;border: 1px solid #ccc;background: #bcbcbc;'>
														".urldecode($bodycode)."
													</td>
												</tr>
												
											</table>
										</div>
										</div>";
			//		return $m;
			*/
			
			$m = "<div style='width:100%; background:#ededed; padding: 18px 0px; font-size: 15px;'>
						<div style='width: 85%; margin: auto; border: 1px solid #9c9c9c; background: #fff; border-radius: 2px; font-family: arial; box-shadow: 0px 0px 4px #9e9e9e;'>
							<table style='width:100%;'>
								<tr>
									<td style='width:30%; vertical-align: top;'>
										<table style='width:100%; border-collapse: collapse;'>
											<tr>
												<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
													From:
												</td>
												<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding-left: 5px;'>
													{$f_name} 
												</td>
											</tr>	
											<tr>
												<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
													DTR Coverage:
												</td>
												<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding-left: 5px;'>
													{$coverage}
												</td>
											</tr>
											{$accom_view}
											<tr>
												<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
													Approved By:
												</td>
												<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding-left: 5px;'>
													{$approvedby}
												</td>
											</tr>
											<tr style=''>
												<td colspan=2 style='text-align: center; padding: 10px 5px;'>
													<a href='".base_url()."dtr/approval/".$vercode."/".$pword."/".$uname."' style='text-decoration:none;'>
														<p style='padding: 15px;
																	text-decoration: none;
																	background: #77ece8;
																	font-size: 16px;
																	margin: 0px auto;
																	width: 83%;
																	color: #17625c;
																	font-weight: bold;
																	border: 1px solid #6ccec7;
																	border-radius: 99px;'> Approve 
														<p>
													</a>
												 <a href='".base_url()."/dtr/forapproval/".$pword."/".$uname."' style='text-decoration:none;'>
												   <p style='padding: 15px;
															margin: 0px auto;
															background: #efefef;
															border: 1px solid #b9b9b9;
															font-size: 16px;
															width: 83%;
															border-radius: 99px;'> View all DTR 
													</p>
													</a>
												</td>
											</tr>
										</table>
									</td>
									<td rowspan=6 style='padding: 10px;border: 1px solid #ccc;background: #eaeaea;'>
										".urldecode($bodycode)."
									</td>
								</tr>
								
							</table>
						</div>
						</div>";
			return $m;
		}
	
		
	}
?>













