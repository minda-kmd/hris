<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	Class Dashboard extends CI_Controller{
		
		public function __construct(){
			parent::__construct();
			$this->load->model('admin_model');
		}

		public function index(){
			
			if($this->session->userdata('is_logged_in') != TRUE){
				redirect('/accounts/login/', 'refresh');
			}else{
				
				$this->load->model('personnel_model');
				$this->load->model('Globalvars');
				$this->load->model("v2main/Globalproc");
			
				$data['title'] = '| Dashboard';
				
				$this->load->model("v2main/Leavemgt");
				$emps = $this->Leavemgt->getemployees();
				
				$data['biometric_id'] 		= $this->session->userdata('biometric_id');
				$data['employee_id'] 		= $this->session->userdata('employee_id');
				$data['usertype'] 			= $this->session->userdata('usertype');
				$data['dbm_sub_pap_id'] 	= $this->session->userdata('dbm_sub_pap_id');
				$data['division_id'] 		= $this->session->userdata('division_id');
				$data['level_sub_pap_div']  = $this->session->userdata('level_sub_pap_div');
				$data['employment_type'] 	= $this->session->userdata('employment_type');
				$data['is_head'] 			= $this->session->userdata('is_head');

				$getemployees = $this->admin_model->getemployees();

				$getareas = $this->admin_model->getareas();

				$users = array();
			
				foreach ($getemployees as $rr) {
					$users[] = array('userid' => $rr->biometric_id , 'name' => $rr->f_name);
				}
				
				// for OT data 
					$sql   = "select e.f_name, p.position_name from employees as e JOIN positions as p on e.position_id = p.position_id where e.employee_id = '{$data['employee_id']}'";
					$ddata = $this->Globalproc->__getdata($sql);
					$data['OT_data'] = [
							"ddate"	    => date("m/d/Y"),
							"dday"	    => date("l"),
							"name"	    => $ddata[0]->f_name,
							"position"  => $ddata[0]->position_name
						];
				// end for OT data 

				$data['areas'] = $getareas;
				$data['admin'] = ($this->Globalvars->usertype != "user")?true:false;

				$data['sub_pap_division_tree'] = $this->personnel_model->getsubpap_divisions_tree();
				$data['dtrformat'] = $this->admin_model->getdtrformat();
				$data['dbusers'] = $getemployees;
				
				$data['headscripts']['style'][0] = base_url()."v2includes/style/leave.cabinet.css";
				$data['headscripts']['js'][0] 	 = base_url()."v2includes/js/leave.calendar.js";
				
				$spl			    			 = $this->Globalproc->get_spl_count($data['employee_id']);
				
				$data['splcount']	 			 = 3 - $spl;
				
				if ($data['employment_type'] == "REGULAR") {
					$remsss						 = $this->Globalproc->getvl_count($data['employee_id']);
					if (count($remsss) > 0) {
						$data['vlcount']			 = ($remsss[0]->vl_value == 0)? "<p class='danger_text'> You have ".$remsss[0]->vl_value." remaining vacation leave. Continue? </p>" : "<p class='ok_elc'>[".$remsss[0]->vl_value."] remaining vacation leave</p>";
						$data['slcount']			 = ($remsss[0]->sl_value == 0)? "<p class='danger_text'> You have ".$remsss[0]->sl_value." remaining sick leave. Continue? </p>" : "<p class='ok_elc'>[".$remsss[0]->sl_value."] remaining sick leave</p>";
						$data['flcount']			 = ($remsss[0]->fl_value == 0)? "<p class='danger_text'> You have ".$remsss[0]->fl_value." remaining forced leave. Continue? </p>" : "<p class='ok_elc'>[".$remsss[0]->fl_value."] remaining forced leave</p>";
					} else {
						$data['vlcount']			 = "<p class='danger_text'> You have 0 remaining vacation leave. Continue? </p>";
						$data['slcount']			 = "<p class='danger_text'> You have 0 remaining sick leave. Continue? </p>";
						$data['flcount']			 = "<p class='danger_text'> You have 0 remaining forced leave. Continue? </p>";
					}
				}
				
				// get all signatories
				$signs 							 = $this->Globalproc->get_signatories( $data['employee_id'] );
				
				$data['isdiv_chief']			 = $this->Globalproc->is_chief("division",$data['employee_id']);
				$data['isdbm_chief'] 			 = $this->Globalproc->is_chief("director",$data['employee_id']);
				// end 
				
				// signatories
				$data['division']			 	 = $signs['division'];
				$data['dbm']			 	 	 = $signs['dbm'];
				// end
				
				// if other signatories
				$data['division_other']			 = $signs['division_other'];
				$data['dbm_other']			 	 = $signs['dbm_other'];
				// end 
				
				// is first time logged in?
					$data['isfirst']			 = $this->Globalproc->gdtf("users",["employee_id"=>$data['employee_id']],["isfirsttime"])[0]->isfirsttime;
				// end 
				
				$data['main_content'] 			 = 'hrmis/dashboard/dashboard_view';
				$this->load->view('hrmis/admin_view',$data);
			}
			
		}
		
		public function cabinet() {
			
		}
		
		public function test_view() {
			$this->load->model("v2main/Globalproc");
			$this->load->model("Globalvars");
			
			if ( $this->Globalproc->is_chief("director",$this->Globalvars->employeeid) ) {
						// get the details of the head of the OC
						$fordir 	 		 = "select email_2, employee_id from employees where DBM_Pap_id = '1' and is_head = '1' and Division_id = '0'";
						$fordir_data 		 = $this->Globalproc->__getdata($fordir);
						$sendto 			 = $fordir_data[0]->email_2;
						$approving_person_id = $fordir_data[0]->employee_id;
						$isfinal 			 = true;
						$subject 			 = "I need your approval";
						$proceed_email 		 = true;
						
					echo $fordir;
					echo "<br/>";
					echo $sendto;
					echo "<br/>";
					echo $approving_person_id;
					echo "<br/>";
					echo $isfinal;
					echo "<br/>";
					echo $subject;
					echo "<br/>";
					echo $proceed_email;
			}
			
		}
		
		public function remaining_spl() {
			$this->load->model('Globalvars');
			$this->load->model("v2main/Globalproc");
			
			$spl  			= $this->Globalproc->get_spl_count($this->Globalvars->employeeid);
			
			echo json_encode( ["count"=>(3-$spl)] );
		}

	}