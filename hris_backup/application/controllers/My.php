<?php

	class My extends CI_Controller {
		protected $myid;

		public function __construct() {
			parent::__construct();
			
			if($this->session->userdata('is_logged_in') != TRUE){
				$s_emp = $this->uri->segment(7);
				$DB2   = $this->load->database('sqlserver', TRUE);
				
				$query = "select * from d_accomplishment where f_signatory = '{$s_emp}'";
				
				$query  = $DB2->query($query);
				$result = $query->result();
				
				if(count($result)==0) {
					$q2 	= "select * from d_accomplishment where s_signatory = '{$s_emp}'";
					$query  = $DB2->query($query);
					$result = $query->result();
					
					if (count($result)==0) {
						die("User not recognized");
						return;
					}
				}
				
				$query = "Select * from users where employee_id = '{$s_emp}'";
				$query = $DB2->query($query);
				
				$result = $query->result();
				
				if (count($result) == 0) {
					die("No user found");
					return;
				}
				
				$this->load->model("Login_model");
				$result2      = $this->Login_model->getUserInformation($s_emp);
				
				$user_session = array(
					'employee_id' => $s_emp,
					'username' => $result[0]->Username,
					'usertype' => $result[0]->usertype,
					'full_name' => $result2[0]->f_name,
					'first_name' => $result2[0]->firstname,
					'last_name' => $result2[0]->l_name,
					'biometric_id' => $result2[0]->biometric_id,
					'area_id' => $result2[0]->area_id,
					'area_name' => $result2[0]->area_name,
					'ip_address' => $_SERVER["REMOTE_ADDR"],
					'is_logged_in' => TRUE,
					'database_default' => 'sqlserver',
					'employment_type' => $result2[0]->employment_type,
					'employee_image' => $result2[0]->employee_image,
					'level_sub_pap_div' => $result2[0]->Level_sub_pap_div,
					'division_id' => $result2[0]->Division_id,
					'dbm_sub_pap_id' => $result2[0]->DBM_Pap_id,
					'is_head' => $result2[0]->is_head,
					'office_division_name' => $result2[0]->office_division_name,
					'position_name' => $result2[0]->position_name
				);
				
				$this->session->set_userdata($user_session);
				
 			}
			
			$this->load->Model("Globalvars");
		
			$this->load->Model("v2main/Actiononleave");

			$this->myid = $this->Globalvars->employeeid;
			
			$this->_upload_docs();
		}

		public function applications() {
			$this->load->model("v2main/Globalproc");

			$sql 		= "select * from leaveapplications as a
							JOIN employees as b 
								on a.empid = b.employee_id
							JOIN leaves as c 
								on a.typeofleave_id = c.leave_id
							where a.empid = '{$this->myid}'";
			
			$data['la'] = $this->Globalproc->__getdata($sql);


			$data['title'] 		  = '| My Applications';
			$data['main_content'] = "v2views/actiononapplication";

			$this->load->view('hrmis/admin_view',$data);
		
		}
		
		public function dashboard() {
			$this->load->model("Globalvars");
			$this->load->model("v2main/Dashboard");
			$this->load->model("v2main/Globalproc");
			
			$data['admin'] = ($this->Globalvars->usertype != "user")?true:false;
			
			$data['title'] = '| My Dashboard';
			
			$data['headscripts']['style'][0]  = base_url()."v2includes/style/hr_dashboard.style.css";
			
			//$data['headscripts']['style'][1]     = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
				
			$data['headscripts']['style'][1]  = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
			
			// for leave management
			$data['headscripts']['style'][2]  = base_url()."v2includes/style/leavemgt.style.css";
			
			$data['headscripts']['js'][0] 	  = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js";
						
			$data['main_content'] = "v2views/hr_dashboard";
			$this->load->view('hrmis/admin_view',$data);
		}
		
		public function ledger($tbl = '', $emp_id = '') {
			$data['title'] = '| My Leave Ledger';

			echo "<script>";
				echo "var isadmin = false;";
			echo "</script>";
			
			echo "<style>";
				echo ".leave_ledger_btn {
							background:#96d0f1;
							color: #333 !important;
						}";
			echo "</style>";
			
			if ($tbl == '') {
				$data['dont_display'] = true;
				$data['headscripts']['style'][0]  = base_url()."v2includes/style/hr_dashboard.style.css";	
				$data['headscripts']['style'][1]  = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
				
				$data['headscripts']['style'][2]  = base_url()."v2includes/style/leavemgt.style.css";
				
				$data['headscripts']['js'][0] 	  = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js";				
				$data['headscripts']['js'][1]     = base_url()."v2includes/js/windowresize.js";				
				$data['headscripts']['js'][2]     = base_url()."v2includes/js/leavemgt.procs.js";
				$data['headscripts']['js'][3] 	  = base_url()."v2includes/js/my_leave_ledger.js";
				
				$data['main_content'] 			  = "v2views/leavemgt";
			} elseif ($tbl == "coc") {
				$this->load->model("v2main/Globalproc");
				$this->load->model("Globalvars");
				
				if ($emp_id == '' || $this->session->userdata("usertype") != "admin") {
					$emp_id = $this->Globalvars->employeeid;
				}
				
				echo "<script>";
					echo "var t_emp_id = '{$emp_id}'";
				echo "</script>";
				
				$data['emp_name'] 				= $this->Globalproc->gdtf("employees",["employee_id"=>$emp_id],["f_name"])[0]->f_name; 
				
				$data['headscripts']['style'][] = base_url()."v2includes/style/ctoot.style.css";
				$data['headscripts']['js'][] 	= base_url()."v2includes/js/ctoot.js";
				$data['main_content'] 			= "v2views/cto_ledger";
			}
			
			$this->load->view('hrmis/admin_view',$data);
		}
		
		public function checking() {
			$this->load->model("Globalvars");
			$this->load->model("v2main/Globalproc");
			
			$logged_in_id = $this->Globalvars->employeeid;
			
			// $emp_details = $this->Globalproc->gdtf("users",["employee_id"=>$logged_in_id],["usertype"]);
			$emp_sql = "select u.usertype, e.f_name from users as u
						JOIN employees as e on u.employee_id = e.employee_id
						where u.employee_id = '{$logged_in_id}'";
			$emp_details = $this->Globalproc->__getdata($emp_sql);
			
			echo json_encode( [$emp_details[0]->usertype , $logged_in_id, $emp_details[0]->f_name] );
		}
		
		public function notify_div_chief() {
			$info   = $this->input->post("info");
			$sum_id = $info['sum_id'];
			
			$this->load->model("v2main/Globalproc");
			
			$details = ["employee_id","date_start_cover","date_end_cover"];
			$where   = ["sum_reports_id"=>$sum_id];
			$data    = $this->Globalproc->get_details_from_table("dtr_summary_reports",$details,$where);
			
			$emp_id  = $data['employee_id'];
			$d_start = $data['date_start_cover'];
			$d_end   = $data['date_end_cover'];	
		}
				
		public function sendNotificationToChief() {
			$this->load->model("Globalvars");
			$this->load->model("v2main/Globalproc");
			$this->load->model("main/Main_model");
			
			$emp_id  		= $this->Globalvars->employeeid;
			$info  			= $this->input->post("info");
			
			$html 			= $info["htmlcode"];
			
			$sum_rep_id     = $info['sum_id'];
			
			/*
			$html       = "%3C%21DOCTYPE%20html%3E%0A%3Chtml%3E%0A%3Chead%3E%0A%3Cmeta%20charset%3D%22utf-8%22%20/%3E%0A%3Cstyle%3E%20%20@media%20print%7B@page%20%7Bsize%3A%20Letter%20Portrait%3B%20%20%7D%7D%20%23body%20table%20th%7B%20border%3A%20none%20%21important%3B%20margin%3A%200px%20%21important%3B%20padding%3A%200px%20%21important%3B%20font-size%3A%2012px%20%21important%3B%7D%20%23body%20table%20tr%20td%7B%20border%3Anone%20%21important%3B%20font-size%3A12px%20%21important%3B%20%20height%3A%2015px%20%21important%3Bmargin%3A%200px%20%21important%3B%20%7D%20%3C/style%3E%3C/head%3E%0A%3Cbody%20style%3D%22margin%3Aauto%3Bfont-family%3Acalibri%3B%20background%3A%20%23efecec%3B%22%3E%0A%0A%3Cdiv%20id%3D%22body%22%20style%3D%22margin-top%3A0px%20%21important%3B%20padding%3A0%3B%20margin%3Aauto%3B%20background%3A%20%23fff%3B%22%3E%3Ctable%20style%3D%22border-collapse%3A%20collapse%3B%22%20cellspacing%3D%220%22%20cellpadding%3D%222%22%3E%0A%09%09%09%3Cthead%3E%0A%09%09%09%09%3Ctr%3E%3Cth%20style%3D%22width%3A%20255px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Aleft%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EESCA%D1O%2C%20CHARLITA%20A%3C/th%3E%0A%09%09%09%09%3Cth%20style%3D%22width%3A%20120px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Acenter%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EIN%3C/th%3E%0A%09%09%09%09%3Cth%20style%3D%22width%3A%20120px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Acenter%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EOUT%3C/th%3E%0A%09%09%09%09%3Cth%20style%3D%22width%3A%20120px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Acenter%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EIN%3C/th%3E%0A%09%09%09%09%3Cth%20style%3D%22width%3A%20120px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Acenter%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EOUT%3C/th%3E%0A%09%09%09%09%3Cth%20style%3D%22width%3A%20120px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Acenter%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EIN%3C/th%3E%0A%09%09%09%09%3Cth%20style%3D%22width%3A%20120px%3B%20font-size%3A13px%3Bfont-weight%3A400%3Bfont-style%3Anormal%3Bbackground-color%3A%23E8E8E8%3Bcolor%3A%23000000%3Bborder-color%3A%23AAAAAA%3Btext-align%3Acenter%3Bborder-top-width%3A0.989583px%3Bborder-left-width%3A0px%3Bborder-right-width%3A0.989583px%3Bborder-bottom-width%3A0px%3Bborder-top-style%3Asolid%3Bborder-left-style%3Asolid%3Bborder-right-style%3Asolid%3Bborder-bottom-style%3Asolid%3Bheight%3A30px%3B%22%3EOUT%3C/th%3E%0A%09%09%09%3C/tr%3E%3C/thead%3E%0A%09%09%09%3Ctbody%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/1/2018%20Th%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A57%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A23%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A33%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A23%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/2/2018%20Fr%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A20%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A09%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A50%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E05%3A32%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/3/2018%20Sa%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/4/2018%20Su%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/5/2018%20Mo%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A01%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A35%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A44%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E05%3A21%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/6/2018%20Tu%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E09%3A04%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A10%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A21%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A20%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/7/2018%20We%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A56%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A17%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A41%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A09%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/8/2018%20Th%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A48%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A05%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A38%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/9/2018%20Fr%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E09%3A04%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A19%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A46%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A23%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/10/2018%20Sa%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/11/2018%20Su%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/12/2018%20Mo%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E07%3A50%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A41%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A06%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/13/2018%20Tu%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E09%3A00%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A25%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A35%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A57%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/14/2018%20We%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E09%3A06%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A32%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A44%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A14%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/15/2018%20Th%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A56%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A20%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/16/2018%20Fr%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/17/2018%20Sa%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/18/2018%20Su%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/19/2018%20Mo%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E08%3A05%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A08%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A20%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E05%3A46%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/20/2018%20Tu%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/21/2018%20We%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/22/2018%20Th%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/23/2018%20Fr%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/24/2018%20Sa%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/25/2018%20Su%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/26/2018%20Mo%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E07%3A53%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A12%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A26%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A38%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/27/2018%20Tu%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E09%3A03%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A08%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A15%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A13%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%09%3Ctr%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E2/28/2018%20We%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E09%3A02%20AM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A21%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E12%3A28%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E06%3A00%20PM%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%09%3Ctd%20style%3D%22border%3A%201px%20solid%20%23ccc%20%21important%3B%20padding%3A%207px%3B%20font-size%3A%2015px%20%21important%3B%22%3E%26nbsp%3B%3C/td%3E%0A%09%09%09%09%3C/tr%3E%0A%09%09%09%3C/tbody%3E%0A%3C/table%3E%3C/div%3E%0A%0A%3C/body%3E%0A%3C/html%3E";
			$emp_id     = 27;
			$sum_rep_id = 2184;
			$chiefid	= 22;
			$dbm_id     = 27;
			*/
			
			$data  	  = $this->Globalproc->get_details_from_table("employees",
																 ["employee_id"=>$emp_id],
																 ["Division_id","DBM_Pap_id","f_name","firstname","l_name","employment_type","area_id"]);
			//var_dump($data);
			$divid 	  = $data['Division_id'];
			
			$fullname = $data['firstname']." ".$data['l_name'];

			// start here =================
				$chiefid  	 = $info['divchief'];
				$dbm_id   	 = $info['dbm'];
				
				if ($this->Globalproc->is_chief("division",$emp_id)) {
					$chiefid = $dbm_id;
				}
				
				if ($this->Globalproc->is_chief("director",$emp_id)) {	
					if ($emp_id == 27) { // doc Cha
						$dbm_id = $chiefid	= 1443; // chairmans ID
					} else if ( $emp_id == 80 || $emp_id == 22 || $emp_id == 59) { // dir Rey or dir Olie or asec Yo, respectively
						$dbm_id = $chiefid	= 1441; // usec's ID
					} else if ( $data['DBM_Pap_id'] == 1 && $data['Division_id']==0 ) { // head of OC
						$dbm_id = $chiefid	= 1443; // chairmans ID
					}
				}
				
				if ($data['area_id'] != 1 && !$this->Globalproc->is_chief("division",$emp_id)) {
					$dbm_id = $chiefid;
				}
				
				//$data2    	 = $this->Globalproc->gdtf("employees",["employee_id"=>$chiefid],"*");
				$data2    	 = $this->Globalproc->gdtf("employees",["employee_id"=>$chiefid],["email_2"]);
				$chief_email = $data2[0]->email_2;
				
				// secret verification code =============================================================================================================
					$vercode  = md5($chief_email).md5( date("mdyhis") ); 
				// end secret verification code =========================================================================================================
				
				// approving body
					$chief_user  = $this->Globalproc->get_details_from_table("users",['employee_id'=>$chiefid],['Username','Password']);
					$c_u 		 = $chief_user['Username'];
					$c_p 		 = $chief_user['Password'];
				// end approving body

			// end here ===================
			
			//echo json_encode($data2);
			
			$sumrep   = $this->Globalproc->get_details_from_table("dtr_summary_reports",["sum_reports_id"=>$sum_rep_id],["dtr_coverage","date_start_cover","date_end_cover","dtr_cover_id"]);
			$coverage = $sumrep["dtr_coverage"];
			$from_    = $sumrep['date_start_cover'];
			$to_      = $sumrep['date_end_cover'];
			$cover_id = $sumrep['dtr_cover_id'];
			
			$accom_view = null;
			// mark attachaccom
			if ($data['employment_type'] == "JO") { 
				$update_accom_sql = "update d_accomplishment 
										set coverage_id = '{$cover_id}', 
											spl_grp_id = '{$sum_rep_id}'
										where date between '{$from_}' and '{$to_}'
										and user_id = '{$emp_id}'";
										
				$update_ 		  = $this->Globalproc->run_sql($update_accom_sql);
				// my/accomplishments/viewing/389/01-29-2018/02-08-2018
				$d_from = date("m-d-Y",strtotime($from_));
				$d_to   = date("m-d-Y",strtotime($to_));
				$accom_view 	  = "<tr>
										<td> </td>
										<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding: 13px; text-align: center;'>
											<a href='".base_url()."/my/accomplishments/viewing/{$emp_id}/{$d_from}/{$d_to}/{$chiefid}'>View Accomplishment Report</a> 
										</td>
									</tr>";
			}
				
			// end mark accom 
			
			// get the division chief
				// if logged in account is division chief... neglect
			
			$bypass_email = false;
			
			//if ($is_initial_approved == false) {
				//if ($emp_id == $chiefid) {
				if ($this->Globalproc->is_chief("division",$emp_id) || $this->Globalproc->is_chief("director",$emp_id) || $data['area_id'] != 1 ) {
					$isapproved 	= $this->Globalproc->__update("dtr_summary_reports",
																 ["is_approved"=>1,"approved_by"=>$chiefid], 
																 ["sum_reports_id"=>$sum_rep_id]);
					
					// $bypass_email   = true;
				} else {
					$isapproved 	= 0;
				}
				
			//	$isapproved 	= 0;

		//	} else {
		//		$ret = true;
		//	}
			
			// for accom 
				$ranges = explode("-",$coverage);
				$from_  = date("m-d-Y", strtotime($ranges[0]));
				$to_    = date("m-d-Y", strtotime($ranges[1]));
			// end for accom

			$m = "	<div style='width:100%; background:#ededed; padding: 18px 0px; font-size: 15px;'>
						<div style='width: 85%; margin: auto; border: 1px solid #9c9c9c; background: #fff; border-radius: 2px; font-family: arial; box-shadow: 0px 0px 4px #9e9e9e;'>
							<table style='width:100%;'>
								<tr>
									<td style='width:30%; vertical-align: top;'>
										<table style='width:100%; border-collapse: collapse;'>
											<tr>
												<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
													From:
												</td>
												<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding: 13px; text-align: center;'>
													{$data['f_name']} 
												</td>
											</tr>	
											<tr>
												<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
													DTR Coverage:
												</td>
												<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding: 13px; text-align: center;'>
													{$coverage}
												</td>
											</tr>
											{$accom_view}
											<tr>
												<td style='width:43%; text-align: right; padding: 10px; border: 1px solid #ccc;'>
													Approved By:
												</td>
												<td style='font-size: 14px;font-weight: bold; border: 1px solid #ccc; padding: 13px; text-align: center;'>
													not yet approved
												</td>
											</tr>
											<tr style=''>
												<td colspan=2 style='text-align: center; padding: 10px 5px;'>
													<a href='".base_url()."dtr/approval/".$vercode."/".$c_p."/".$c_u."' style='text-decoration:none;'>
														<p style='padding: 15px;
																	text-decoration: none;
																	background: #77ece8;
																	font-size: 16px;
																	margin: 0px auto;
																	width: 83%;
																	color: #17625c;
																	font-weight: bold;
																	border: 1px solid #6ccec7;
																	border-radius: 99px;'> Approve 
														<p>
													</a>
												 <a href='".base_url()."/dtr/forapproval/".$c_p."/".$c_u."' style='text-decoration:none;'>
												   <p style='padding: 15px;
															margin: 0px auto;
															background: #efefef;
															border: 1px solid #b9b9b9;
															font-size: 16px;
															width: 83%;
															border-radius: 99px;'> View all DTR 
													</p>
													</a>
												</td>
											</tr>
										</table>
									</td>
									<td rowspan=6 style='padding: 10px;border: 1px solid #ccc;background: #eaeaea;'>
										".urldecode($html)."
									</td>
								</tr>
								
							</table>
						</div>
						</div>"; //  {$cntid[0]->cntid}/{$sum_rep_id}/{$c_u}/{$c_p}

			$ret 	  = false;
			$ret 	  = $this->Globalproc->__save("countersign", ["bodycode" 	   	  => $html,  //urlencode($m),
																  "emp_id"   	   	  => $emp_id,
																  "approval_status"   => $isapproved,
																  "tobeapprovedby"    => $chiefid,
																  "last_approving"	  => $dbm_id,
																  "dtr_summary_rep"   => $sum_rep_id,
																  "vercode" 		  => $vercode,
																  "hrnotified"		  => 0]);
			$cntid    = $this->Globalproc->getrecentsavedrecord("countersign", "cntid");
				
			if($ret && $bypass_email == false) {
				
				//$chief_email = trim($chief_email," "); '"'.
				$ret = $this->Globalproc->sendtoemail(["to"		 => $chief_email, 
													   "from"	 => $fullname,
													   "subject" => "DTR: For approval",
													   "message" => $m]); 
				
				
				$as_dets = [
					"asdtr_sumrep"	=> $sum_rep_id,
					"cnt_id"		=> $cntid[0]->cntid,
					"emp_id"		=> $emp_id,
					"status"		=> 0
				];
				
				$ret = $this->Globalproc->__save("allowedsubmit",$as_dets);
				/*
				$msg_p = "<div style='width: 100%;
									  background: #e0e0e0;
									  padding: 7px; font-size: 18px;'>
									<div style='width: 30%;
												margin: 10px auto;
												background: #fff;
												border: 1px solid #ccc;'>
										<div style='padding:10px;padding: 22px;background: #f1f1f1;border-bottom: 1px solid #ccc;'>
											<h3 style='margin:0px; font-family: calibri;'> DTR for your perusal </h3>
										</div>
										<div style='padding: 17px; font-family: calibri;'>
											<p style='margin:0px;'> Hi HR, </p>
											<p> Im sending to you my DTR for your evaluation. </p>
											<span> Regards, </span> <br/>
											<span> {$fullname} </span>
										</div>
										<div style='padding: 13px;
													background: #f1f1f1;
													border-top: 1px solid #ccc;'>
											<p style='margin:0px; text-align:center; font-family: calibri;'> Please <a href='#'>login</a> to your HRIS  </p>
										</div>
									</div>
								</div>";
				
				if ($ret) {
					$ret = $this->Globalproc->sendtoemail(["to"		 => "hr@minda.gov.ph",//"hr@minda.gov.ph",
														   "from"	 => $fullname,
														   "subject" => "DTR: for your perusal",
														   "message" => $msg_p]);
				}
				*/
				
			}
			
			// ==========================
				// record to activity
					
				// end
			// ==========================
			
			echo json_encode($ret);
			
		}
		
		public function dtr() { // personal dtr of each employee
			$this->load->model("personnel_model");
			$this->load->model("admin_model");
			$this->load->model("v2main/Globalproc");
			
			$data['title'] = '| Daily Time Records';
			
			$this->load->model("Globalvars");
			$data['admin'] = ($this->Globalvars->usertype != "user")?true:false;
			
			
			if($this->session->userdata('is_logged_in')!=TRUE){
				  redirect('/accounts/login/', 'refresh');
			}else{

			$data['biometric_id'] 	   = $this->session->userdata('biometric_id');
			$data['employee_id'] 	   = $this->session->userdata('employee_id');
			$data['usertype'] 		   = $this->session->userdata('usertype');
			$data['dbm_sub_pap_id']    = $this->session->userdata('dbm_sub_pap_id');
			$data['division_id'] 	   = $this->session->userdata('division_id');
			$data['level_sub_pap_div'] = $this->session->userdata('level_sub_pap_div');
			$data['employment_type']   = $this->session->userdata('employment_type');
			$data['is_head'] 		   = $this->session->userdata('is_head');
			
			
			$empdata 					  = $this->Globalproc->get_details_from_table("employees",
																					 ["employee_id"=>$data['employee_id']],
																					 ['e_signature','area_id']);
			
			$data['signature']["emp_sig"] = $empdata['e_signature'];
			
		//	$chief = $this->Globalproc->gdtf("dtr_summary_reports",[''], $details)
			// chief_sig	
				
			$getemployees = $this->admin_model->getemployees();

			$getareas = $this->admin_model->getareas();

			$users = array();
		
			foreach ($getemployees as $rr) {
				$users[] = array('userid' => $rr->biometric_id , 'name' => $rr->f_name);
			}

			$data['areas'] 		 = $getareas;
			$data['margin_left'] = true;

			$data['sub_pap_division_tree'] = $this->personnel_model->getsubpap_divisions_tree();
			$data['dtrformat'] 	 		   = $this->admin_model->getdtrformat();
			$data['dbusers'] 	 		   = $getemployees;
			
			$data['signatories'] = $this->Globalproc->get_signatories($data['employee_id']);

			$data['ischief']	 = $this->Globalproc->is_chief("division",$data['employee_id']);
			$data['isdirector']	 = $this->Globalproc->is_chief("director",$data['employee_id']);
			$data['isamo']		 = $empdata['area_id'];
			

			// check if has an unsubmitted DTR
				// check in countersign 
				$prev_dtr_status = $this->Globalproc->check_prev_dtr( $data['employee_id'] );
				$coverage = $this->Globalproc->checkforactive_coverage($data['employment_type']);
				
				if (count($prev_dtr_status)>0) {
					echo "<div class='notification_div'>";
					echo "<div class='hrnotify_div'>";
						//echo "<p> You have a pending DTR submitted to HR. You are only allowed to submit again when the previous one is allowed to go. </p>";
						echo "<p> <i class='fa fa-exclamation-triangle'></i> &nbsp; You have a pending DTR. <br/>
								<span style='font-size: 14px;'> Reminder: Please settle your DTR before the deadline or you wont be able to submit it, therefore you won't get paid for your job. </span>	
							 </p>";
					echo "</div>";
					
					// look for some findings in the findings table 
						$findings = $this->Globalproc->gdtf("allowedsubmit",["emp_id"=>$data['employee_id'],"conn"=>"and","status"=>1],"*");
						
						if (count($findings)>0) {
							$the_findings = json_decode($findings[0]->findings);
							$evaluator    = $this->Globalproc->gdtf("employees",['employee_id'=>$findings[0]->correctedby],"f_name")[0]->f_name;
							
							echo "<div class='boxwrapper' style='overflow:hidden;'>";
								echo "<div class='box_findings'>";
									echo "<h4 class='header_ff'> Your DTR needs fixing. Please fix the following </h4>";
									echo "<ul>";
										for($i = 0; $i <= count($the_findings)-1; $i++) {
											echo "<li>";
												echo "<strong> {$the_findings[$i]->date} </strong>";
												echo "<p> {$the_findings[$i]->msg} </p>";
											echo "</li>";
										}
									echo "</ul>";
									//05/04/2018 :: format
									// check if allowed to submit or not or the deadline has met
																		
									//echo $coverage[0]->is_allow_to_submit;
									if ($coverage[0]->is_allow_to_submit == 1 && $coverage[0]->date_deadline >= date("m/d/Y")) {
										echo "<button class='btn btn-primary' style='margin: 10px 0px;' 
													  id='resubmittohr' 
													  data-as_id = '{$findings[0]->as_id}' 
													  data-db_from = '{$findings[0]->from_db}'> Send Back </button>";
									} else {
										echo "<p class='errormsg_text'> I'm sorry, deadline have passed but you haven't submitted your DTR. The submission is now closed. </p>";
									}
									// end
									
									echo "<p class='evaluatedby'> evaluated by: {$evaluator}</p>";
								echo "</div>";		
							echo "</div>";		
						}
					// end 
					
					$email_status  = $this->Globalproc->gdtf("countersign",["countersign_id"=>$prev_dtr_status[0]->countersign_id],["approval_status","tobeapprovedby","last_approving"]);
					if ($email_status[0]->approval_status == 0) { // not approved by chief, send to chief
						echo "<div class='notyetapproved_initial'>";
							echo "<p id='sendtochief' 
									 data-cntid = '{$prev_dtr_status[0]->countersign_id}'
									 data-sendto = '{$email_status[0]->tobeapprovedby}'> Your submitted DTR is not yet signed by your chief. Click here to notify. </p>";
						echo "</div>";
					} else if ($email_status[0]->approval_status == 1) { // approved by chief, send to last approving official
						echo "<div class='notyetapproved_final'>";
							echo "<p id='sendtolast' 
									 data-cntid = '{$prev_dtr_status[0]->countersign_id}'
									 data-sendto = '{$email_status[0]->last_approving}'> Your submitted DTR is for final signature and is not yet signed. Click here to notify. </p>";
						echo "</div>";
					}
					
					echo "</div>";
					
					$data['cantsubmit'] = true;
				}
			// 
			
			$data['headscripts']['style'][0]  = base_url()."v2includes/style/hr_dashboard.style.css";
			
			echo "<div class='showwindow' id='showwindow'>
						<div id='pop_upwindow'>
							
						</div>
					</div>";
					
			$data['main_content'] = 'v2views/dtr_new_view';
			$this->load->view('hrmis/admin_view',$data);


			}
		}
		
		public function documents() {
			$data['title'] = '| My Documents';
			
			$data['headscripts']['js'][0] 		= base_url()."v2includes/js/upload.js";
			$data['headscripts']['style'][0] 	= base_url()."v2includes/style/documents.style.css";
			
			$data['main_content'] = 'v2views/documents';
			$this->load->view('hrmis/admin_view',$data);
		}
		
		public function _upload_docs() {
			if (isset($_POST['uploaddocs'])) {
				$target_dir = "uploads/";
				$target_file = $target_dir . basename($_FILES["attachments_"]["name"]);
				
			}
		}
		
		public function uploads() {
			// called from an ajax request

			if ( 0 < $_FILES['file']['error'] ) {
				echo 'Error: ' . $_FILES['file']['error'] . '<br>';
			}
			else {
				move_uploaded_file($_FILES['file']['tmp_name'], base_url().'assets/documents/' . $_FILES['file']['name']);
			}
			echo json_encode("uploaded");

		}
		
		public function remaining() {
			$this->load->model("v2main/Globalproc");
			
			$details = $this->input->post("info");
			
			$empid   = $details['empid']; // 62
			$type    = $details['type']; //"FL";
			
			/*
			$empid   = 62; // 62
			$type    = "FL"; //"FL";
			*/
			
			$ret = $this->Globalproc->return_remaining($type,$empid);
			echo json_encode($ret);
			
		}
		
		public function remaining_spl() {
			
		}
		
		public function attach_accom() {
			$accom = $this->input->post("info");
			$accom = $accom['details'];
			
			$this->load->model("v2main/Globalproc");
			$this->load->model("Globalvars");
		
			$cover_id   = $accom["coverid"];
			$from_ 		= $accom["from_"];
			$to_ 		= $accom["to_"];
			
			$user_id    = $this->Globalvars->employeeid;
			
			// __createuniqueid($word)
			$spl_grp_id = $this->Globalproc->__createuniqueid($cover_id.$from_.$to_);
			
			$update_accom_sql = "update d_accomplishment 
									set coverage_id = '{$cover_id}', 
										spl_grp_id = '{$spl_grp_id}'
									where date between '{$from_}' and '{$to_}'
									and user_id = '{$user_id}'";
			
			$update_ = $this->Globalproc->run_sql($update_accom_sql);
			echo json_encode($update_);
			
		}
		
		public function accomplishments($isprint = '', $user_id = '', $r_from = '', $r_to = '', $s_emp = '') {
			/*
			if($this->session->userdata('is_logged_in') != TRUE){
				$DB2   = $this->load->database('sqlserver', TRUE);
				
				$query = "select * from d_accomplishment where f_signatory = '{$s_emp}'";
				
				$query  = $DB2->query($query);
				$result = $query->result();
				
				if(count($result)==0) {
					$q2 	= "select * from d_accomplishment where s_signatory = '{$s_emp}'";
					$query  = $DB2->query($query);
					$result = $query->result();
					
					if (count($result)==0) {
						die("User not recognized");
						return;
					}
				}
				
				$query = "Select * from users where employee_id = '{$s_emp}'";
				$query = $DB2->query($query);
				
				$result = $query->result();
				
				if (count($result) == 0) {
					die("No user found");
					return;
				}
				
				$this->load->model("Login_model");
				$result2      = $this->Login_model->getUserInformation($s_emp);
				
				$user_session = array(
					'employee_id' => $s_emp,
					'username' => $result[0]->Username,
					'usertype' => $result[0]->usertype,
					'full_name' => $result2[0]->f_name,
					'first_name' => $result2[0]->firstname,
					'last_name' => $result2[0]->l_name,
					'biometric_id' => $result2[0]->biometric_id,
					'area_id' => $result2[0]->area_id,
					'area_name' => $result2[0]->area_name,
					'ip_address' => $_SERVER["REMOTE_ADDR"],
					'is_logged_in' => TRUE,
					'database_default' => 'sqlserver',
					'employment_type' => $result2[0]->employment_type,
					'employee_image' => $result2[0]->employee_image,
					'level_sub_pap_div' => $result2[0]->Level_sub_pap_div,
					'division_id' => $result2[0]->Division_id,
					'dbm_sub_pap_id' => $result2[0]->DBM_Pap_id,
					'is_head' => $result2[0]->is_head,
					'office_division_name' => $result2[0]->office_division_name,
					'position_name' => $result2[0]->position_name
				);
				
				$this->session->set_userdata($user_session);
				
 			}
			*/
			
			$this->load->model('Globalvars');
			$this->load->model("Globalproc");
			$this->load->model('main/main_model');
						
			$data['title'] = "| Accomplishment";
			
			$data['headscripts']['style'][] = base_url()."v2includes/style/accomplishments.style.css";
						
			// get all signs
			$get_from = $this->Globalvars->employeeid;
			if ($user_id != '') {
				$get_from = $user_id;
			}
			
			$signs 		= $this->Globalproc->get_signatories( $get_from );
			
			// signatories
			$data['division']		= $signs['division'];
				$div_sql 			= "select position_name, Division_Desc from employees as e 
									   JOIN positions as p on e.position_id = p.position_id 
									   JOIN Division as d on e.Division_id = d.Division_Id
									   where e.employee_id = '{$signs['division']['div_empid']}'";
				$data['div_data']   = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->__getdata($div_sql) );
			
			$data['dbm']			= $signs['dbm'];
				$dbm_sql 			= "select position_name, DBM_Sub_Pap_Desc from employees as e 
									   JOIN positions as p on e.position_id = p.position_id 
									   JOIN DBM_Sub_Pap as d on e.DBM_Pap_id = d.DBM_Sub_Pap_id
									   where e.employee_id = '{$signs['dbm']['dbm_empid']}'";
				
				$data['dbm_data']   = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->__getdata($dbm_sql) );
				
			// if other than signatories
			$data['division_other']			 = $signs['division_other'];
			$data['dbm_other']			 	 = $signs['dbm_other'];
			
			$month 	  = date("n");
			$day   	  = date("j");
			$year  	  = date("Y");
			
				if (isset($_POST['view_accom'])) {
					$month 	  = $_POST['month_select'];
					$day   	  = $_POST['day_select'];
					$year  	  = $_POST['year_select'];
					
				}
			
			$data['selected_day']   = $day;
			$data['selected_year']  = $year;
			$data['selected_month'] = $month;
			
			$the_date = $month."/".$day."/".$year;
			
			
			$data['accom_this_day'] = $this->main_model->array_utf8_encode_recursive($this->Globalproc->gdtf("d_accomplishment",
																					 ["date"	=> $the_date,
																					  "conn"    => "and",
																					  "user_id" => $this->Globalvars->employeeid],
																					  "*") );
			
			// get approval status
			$data['div_sig'] 		= null;
			$data['dbm_sig']		= null;
			
			// end get the approval status
			
				if (isset($isprint) && $isprint == "print") {
					$data['headscripts']['js'][]  = base_url()."v2includes/js/accomprint.js";
					
					// personal information 
						$personal_sql  		   = "select f_name,position_name,e_signature from employees as e JOIN positions as p on e.position_id = p.position_id where e.employee_id = '{$this->Globalvars->employeeid}'";
						$data['personal_info'] = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->__getdata($personal_sql) );
					// end 
					
					if (isset($_POST['print_accom'])) {
						$from_ 	= date("m/d/Y", strtotime($_POST['from_']));
						$to_    = date("m/d/Y", strtotime($_POST['to_']));
						
						$sql = "select * from d_accomplishment where date between '{$from_}' AND '{$to_}' and user_id = '{$this->Globalvars->employeeid}' order by date ASC";
						
						$data['accomplishments'] = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->__getdata($sql) );
						
						//=================================================================================================
							if ($data['accomplishments'][0]->f_action == 1) {
								$data['div_sig'] = $this->Globalproc->gdtf("employees",
																		  ['employee_id'=>$data['accomplishments'][0]->f_signatory],
																		   'e_signature')[0]->e_signature;
							}
								
							if ($data['accomplishments'][0]->s_action == 1) {
								$data['dbm_sig'] = $this->Globalproc->gdtf("employees",
																			['employee_id'=>$data['accomplishments'][0]->s_signatory],
																			 'e_signature')[0]->e_signature;
							}
						//=================================================================================================
						
						$emp_data 			= $this->Globalproc->gdtf("employees",['employee_id'=>$get_from],['e_signature','area_id']);
						$data['emp_sig']	= $emp_data[0]->e_signature;
						$data['area']		= $emp_data[0]->area_id;
							
						$dtr_coverage = "select * from hr_dtr_coverage as hdc
										where hdc.is_active = 'true' and is_allow_to_submit = 'true' ";
						
						$data['from_']     = $from_;
						$data['to_'] 	   = $to_; 
						$data['get_cover'] = $this->Globalproc->__getdata($dtr_coverage);
						
					}
					
					$data['main_content'] 		  = 'v2views/accom_print';
				} else if (isset($isprint) && $isprint == "viewing") {
					// allow no password here
					// date from 
					// date to
					// user id
					// $user_id = '', $r_from = '', $r_to = ''
				// http://office.minda.gov.ph:9003/my/accomplishments/viewing/389/01-29-2018/02-08-2018
					//$from_  = date("m/d/Y", strtotime($r_from));
					//$to_ 	= date("m/d/Y", strtotime($r_to));

					$from_  = $r_from;
					$to_    = $r_to;

					// $user_id = "389";
					// personal information 
						$personal_sql  		   = "select f_name,position_name,e_signature, area_id from employees as e JOIN positions as p on e.position_id = p.position_id where e.employee_id = '{$user_id}'";
						$data['personal_info'] = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->__getdata($personal_sql) );
						$data['area']		   = $data['personal_info'][0]->area_id;
					// end

					$sql = "select * from d_accomplishment where date between '{$from_}' AND '{$to_}' and user_id = '{$user_id}' order by date ASC";
					
					$data['accomplishments'] 	  = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->__getdata($sql) );
					
					//=================================================================================================
						if ($data['accomplishments'][0]->f_action == 1) {
								$data['div_sig'] = $this->Globalproc->gdtf("employees",
																		  ['employee_id'=>$data['accomplishments'][0]->f_signatory],
																		   'e_signature')[0]->e_signature;
						}
								
						if ($data['accomplishments'][0]->s_action == 1) {
							$data['dbm_sig'] = $this->Globalproc->gdtf("employees",
																		['employee_id'=>$data['accomplishments'][0]->s_signatory],
																		 'e_signature')[0]->e_signature;
						}
					//=================================================================================================
					$data['emp_sig']	= $this->Globalproc->gdtf("employees",['employee_id'=>$get_from],"e_signature")[0]->e_signature;
					
					$data['isviewing']			  = true;
					$data['main_content'] 		  = 'v2views/accom_print';
				} else {
					$data['headscripts']['js'][]  = base_url()."v2includes/js/accom.js";
					$data['main_content'] = 'v2views/accomplishments';
				}
			// end
			
			
			// if saving of accomplishments is set
				if (isset($_POST['proceed_accom_save'])) {
					
					
					$month = $_POST['month_select'];
					$day   = $_POST['day_select'];
					$year  = $_POST['year_select'];
					
					$accom_text = urlencode($_POST['accom_text']);
										
					$division   = $_POST['division_certified'];
					$dbm 		= $_POST['dbm_approved'];
					
					$thedate 	= $month."/".$day."/".$year;
					$values = [
						"date" 				=> $thedate,
						"accomplishment"	=> $accom_text,
						"user_id"			=> $this->Globalvars->employeeid,
						"f_signatory"		=> $division,
						"s_signatory"		=> $dbm
					];
					
					$info = $this->Globalproc->gdtf("d_accomplishment",["date"=>$thedate,"conn"=>"and","user_id"=>$this->Globalvars->employeeid],"*");
					
					if (count($info)>=1) {
						// update
						$data['saving_message'] = $this->Globalproc->__update("d_accomplishment",$values,['user_id'=>$this->Globalvars->employeeid,"conn"=>"and","date"=>$thedate]);
					} else {
						// save 
						$data['saving_message'] = $this->Globalproc->__save("d_accomplishment",$values);
					}
				}
			// end 
			
		
			
			$this->load->view('hrmis/admin_view',$data);
		}
		
		public function removeaccom() {
			$this->load->model("Globalproc");
			$this->load->model("Globalvars");
			
			$thedate = $this->input->post("info")['thedate'];
			$userid  = $this->Globalvars->employeeid;
			
			$sql = "DELETE from d_accomplishment where user_id = '{$userid}' and date='{$thedate}'";
			
			echo json_encode( $this->Globalproc->run_sql($sql) );
		}
		
		public function getcto_ot() {
			$this->load->model("Globalproc");
			
			$a 		= $this->input->post("info");
			$empid  = $a['empid'];
			
		//	$empid = 62;
			
			$sql   = "select eot.*,e.f_name from employee_ot_credits as eot
					  JOIN employees as e on eot.emp_id = e.employee_id
					  where eot.emp_id = '{$empid}' ORDER BY eot.elc_otcto_id DESC";
			$data  = $this->Globalproc->__getdata($sql);
			
			/*
			$data = [1,2,3,4,5];
			$b = array_map(function(){
				return "helo worl";
			},$data);
			
			var_dump($b);
			*/
			echo json_encode($data);
		}
		
		public function save_ot_accom() {
			$data 		  = $this->input->post("info");
			$data 		  = $data['ssave'];
			
			$grp_id 	  = $data['grp_id'];
			$am_timein    = $data['am_timein'];
			$am_timeout   = $data['am_timeout'];
			$pm_timein    = $data['pm_timein'];
			$pm_timeout   = $data['pm_timeout'];
			$acc_report   = $data['acc_report'];
			
			/*
			$acc_report  = "%3Cul%3E%3Cli%3E%u200Byuiyui%3Cbr%3E%3C/li%3E%3Cli%3Eyuiyu%3C/li%3E%3C/ul%3E";
			$am_timein   = "12:00 AM";
			$am_timeout  = "12:00 AM";
			$grp_id		 = "1108";
			$pm_timein	 = "12:00 AM";
			$pm_timeout  = "12:00 AM";
			*/
			
			$this->load->model("v2main/Globalvars");
			$this->load->model("v2main/Globalproc");
			
			$empid  	  = $this->Globalvars->employeeid;
			
			// personal information 
				$personal_info = $this->Globalproc->gdtf("employees",["employee_id"=>$empid],["f_name","Division_id"]);
			// end 
			
			
			
			// get the approval status of OT
			// ================================================
				$dets = $this->Globalproc->gdtf("checkexact_ot",["checkexact_ot_id"=>$grp_id],["div_is_approved",
																							   "div_chief_id",
																							   "act_div_is_approved",
																							   "act_div_chief_id",
																							   "act_div_a_is_approved",
																							   "act_div_a_chief_id"]);
				if ($dets[0]->div_is_approved == 1) { // approved: chief level
					if ($dets[0]->act_div_is_approved == 1) { // approved: second level
						if ($dets[0]->act_div_a_is_approved == 1) {	// approved: last level
							if ($dets[0]->div_chief_id == $empid) {
								// send to last approving official
							}
							
							$approval_status = null;
							
							if ($personal_info[0]->Division_id == 0 
								|| $this->Globalproc->is_chief("division",$empid) 
								|| $this->Globalproc->is_chief("director",$empid)) {
								$approval_status = 1;
							} else {
								$approval_status = 0;
							}
							
							$details = [
								"am_timein"  	  => $am_timein,
								"am_timeout" 	  => $am_timeout,
								"pm_timein"  	  => $pm_timein,
								"pm_timeout" 	  => $pm_timeout,
								"accomplishment"  => $acc_report,
								"approval_status" => $approval_status,
								"fapproval"		  => $dets[0]->div_chief_id,
								"sapproval"		  => $dets[0]->act_div_chief_id,
								"lapproval"		  => $dets[0]->act_div_a_chief_id,
								"emp_id"		  => $empid,
								"ot_exact_id"	  => $grp_id,
								"date_added"	  => date("m/d/Y")
							];
							
							$ret = $this->Globalproc->__save("ot_accom",$details);
							
							if ($ret) {
								$ret_id = $this->Globalproc->getrecentsavedrecord("ot_accom", "returned_id");
								$ret_id = $ret_id[0]->returned_id;
								
								$approving_id = null;
								// email 
								$this->load->model("v2main/Emailtemplate");
								$email_dets = [
										"Type"	    			 => "Overtime Accomplishment Report",
										"Name"	    			 => $personal_info[0]->f_name,
										"Date"	    			 => date("m/d/Y"),
										"Work Accomplishment(s)" => urldecode($acc_report)
									]; 
								
								if ($am_timein != null || $am_timeout != null) {
									$email_dets['Morning'] 		= $am_timein." | ".$am_timeout;
								}
								
								if ($pm_timein != null || $am_timeout != null) {
									$email_dets['Afternoon']	= $pm_timein." | ".$pm_timeout;
								}
								
								$details = [
									"to"	  => null,
									"from"	  => strtoupper($personal_info[0]->f_name),
									"subject" => "Overtime Accomplishment Report: Needs Approval ",
									"message" => null,
								];
								
								if ($approval_status >= 1) {
									// send to sapproval or lapproval
									$deta = $this->Globalproc->gdtf("employees",["employee_id"=>$dets[0]->act_div_a_chief_id],["email_2"]);
									$details['to']	= $deta[0]->email_2;
									$approving_id	= $dets[0]->act_div_a_chief_id;
								} else if ($approval_status == 0) {
									$deta 			= $this->Globalproc->gdtf("employees",["employee_id"=>$dets[0]->div_chief_id],["email_2"]);
									$details['to']	= $deta[0]->email_2;
									$approving_id	= $dets[0]->div_chief_id;
								}
								
								// approval($ot_exact = '', $approving_id = '', $ot_accom_id = '')
								$email_actions = [
												"ot_exact"	    => $grp_id,
												"approving_id"  => $approving_id,
												"ot_accom_id"  	=> $ret_id
											];
											
								$template 			= $this->Emailtemplate->ot_accom_template($email_dets,$email_actions);
								$details['message']	= $template;
								
								$emailed = $this->Globalproc->sendtoemail($details);
								if ($emailed) {
									echo json_encode(true);
								}
							}
						} else {
							echo json_encode(false);
						}
					} else {
						echo json_encode(false);
					}
				} else {
					echo json_encode(false);
				}
			// ================================================
			// end 
			
		}

		function getot_accom() {
			$ot_accom = $this->input->post("info");
			$ot_accom = $ot_accom['otaccom'];
			
			$this->load->model("v2main/Globalproc");
			
			$dets = $this->Globalproc->gdtf("ot_accom",["ot_exact_id"=>$ot_accom],"*");
			
			echo json_encode($dets);
		}
		
		function delete_accom() {
			$ot_accid = $this->input->post("info");
			$ot_accid = $ot_accid['ot_accid'];
			
			$this->load->model("v2main/Globalproc");
			
			$del = "Delete from ot_accom where ot_accid = '{$ot_accid}'";
			
			$ret = $this->Globalproc->run_sql($del);
			
			echo json_encode($ret);
		}
		
		function changepass() {
			$info = $this->input->post("info");
			$pass = md5($info['pw']);
			
			$this->load->model("v2main/Globalvars");
			$this->load->model("v2main/Globalproc");
			
			$empid = $this->Globalvars->employeeid;
			
			$update = $this->Globalproc->__update("users",["Password"=>$pass,"isfirsttime"=>0],["employee_id"=>$empid]);
			
			echo json_encode($update);
		}
		
		function imokay() {
			$info = $this->input->post("info");
			$ok   = $info['ok'];
			
			$this->load->model("v2main/Globalvars");
			$this->load->model("v2main/Globalproc");
			
			$empid = $this->Globalvars->employeeid;
			$update = $this->Globalproc->__update("users",['isfirsttime'=>2],['employee_id'=>$empid]);
			
			echo json_encode($update);
		}
		
		function ot($accom = '', $ot_exact = '') {
			if ($accom == 'accomplishment' && $ot_exact != '') {
				$data['title'] 		  = '| My Overtime Accomplishment';
				$data['main_content'] = "v2views/forms/otaccom_report";
				
				$data['headscripts']['style'][0] = base_url()."v2includes/style/otaccom.style.css";
				
				$this->load->model("v2main/Globalproc");
				$data['details'] = $this->Globalproc->gdtf("ot_accom",['ot_exact_id'=>$ot_exact],'*');
				
				$empid 		= $data['details'][0]->emp_id;
				
				$f_approval = null;
				$f_name 	= null;
				
				$s_approval = null;
				$s_name 	= null;
				
				$l_approval = null;
				$l_name 	= null;
				
					if ($data['details'][0]->approval_status == 1 ) {
						if ($this->Globalproc->is_chief("division",$empid) || $this->Globalproc->is_chief("director",$empid)) {
							$f_approval == null;
							$f_name 	= null;
						} else {
							$dets       = $this->Globalproc->gdtf("employees",['employee_id'=>$data['details'][0]->fapproval],['e_signature','f_name']);
							$f_approval = $dets[0]->e_signature;
							$f_name     = $dets[0]->f_name;
						}
					} else if ($data['details'][0]->approval_status == 2 ) {
						if ($this->Globalproc->is_chief("division",$empid) || $this->Globalproc->is_chief("director",$empid)) {
							$f_approval = null;
							$f_name 	= null;
						} else {
							$dets 	 	= $this->Globalproc->gdtf("employees",['employee_id'=>$data['details'][0]->fapproval],['e_signature','f_name']);
							$f_approval = $dets[0]->e_signature;
							$f_name 	= $dets[0]->f_name;
						}
						$ldets 			= $this->Globalproc->gdtf("employees",['employee_id'=>$data['details'][0]->lapproval],['e_signature','f_name']);
						$l_approval		= $s_approval = $ldets[0]->e_signature;
						$l_name 		= $s_name 	  = $ldets[0]->f_name;
					}
				
				$data['signatories'] = [
						'fapproval'=> ['fname'=>$f_name , 'signature' => $f_approval],
						'sapproval'=> ['fname'=>$s_name , 'signature' => $s_approval],
						'lapproval'=> ['fname'=>$l_name , 'signature' => $l_approval]
					];
					
				$personal 			= $this->Globalproc->gdtf("employees",['employee_id'=>$data['details'][0]->emp_id],['e_signature','f_name','position_id']);
				$position 			= $this->Globalproc->gdtf("positions",['position_id'=>$personal[0]->position_id],["position_name"]);
				$data['position']	= $position[0]->position_name;
				
				$data['personal'] = [
					"fname"	=> $personal[0]->f_name,
					"sign"	=> $personal[0]->e_signature
				];
					
				$this->load->view('hrmis/admin_view',$data);
			} else {
				die("The page is not available");
				return;
			}
			
		}
						
		function mytest() {
			$dets = [
				"type"	=> "hi",
				"test"	=> "world",
				"third" => "hello"
			];
			
			foreach($dets as $key => $val) {
				echo $key."=".$val;
				echo "<br/>";
			}
		}
		
		function getlogins() {
			$this->load->model("v2main/Globalproc");
			$this->load->model('main/main_model');
			
			$sql  = "select * from users as u JOIN employees as e on u.employee_id = e.employee_id where u.isfirsttime = '2' and e.status = '1'";
			$data = $this->main_model->array_utf8_encode_recursive($this->Globalproc->__getdata($sql));
			
			echo "<table>";
			foreach($data as $a) {
				echo "<tr>";
					echo "<td>".$a->f_name."</td>";
					echo "<td>".$a->email_2."</td>";
				echo "</tr>";
			}
			echo "</table>";
		}
	}
?>                                                                                                                                                                                                      