<?php 
	
	class Hr extends CI_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->model('admin_model');
			$this->load->model('attendance_model');
			$this->load->model('reports_model');
			$this->load->model('personnel_model');
			$this->load->model('leave_model');
			
		}
		
		function dashboard() {
			$this->load->model("Globalvars");
			$this->load->model("v2main/Dashboard");
			$this->load->model("v2main/Globalproc");
			
			$data['admin'] = ($this->Globalvars->usertype != "user")?true:false;
			
			if ($data['admin'] == false) {
				die("You are not allowed in here...");
			}
			
			$data['title'] = '| Human Resource Dashboard';
			
			$data['headscripts']['style'][0]  = base_url()."v2includes/style/hr_dashboard.style.css";
			
			//$data['headscripts']['style'][1]     = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
				
			$data['headscripts']['style'][1]  = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";
			
			// for leave management
			$data['headscripts']['style'][2]  = base_url()."v2includes/style/leavemgt.style.css";
			
			$data['headscripts']['js'][0] 	  = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js";
			
			$isgetfrom = $this->uri->segment(3);
			
			$sql     = "select * from areas";
			$syncs   = $this->Globalproc->__getdata($sql);
			
			$data['syncs'] = [];
			foreach($syncs as $ss) {
				$last_update  = $this->attendance_model->get_attemndance_logs_last_update($ss->area_id); // mark 2
				
				if (count($last_update)==0) {
					$last_update = "Unknown";
				} else {
					$last_update  = $last_update[0]->last_update;
				}
				
				$loc = [];
				$loc['name']		= $ss->area_name;
				$loc['ip']			= $ss->ipaddress;
				$loc['code']		= $ss->area_code;
				$loc['last_update']	= $last_update;
				array_push($data['syncs'],$loc);
			}
			
			if ($isgetfrom == null) { // getfrom value from url is not present
				// javascript responsible for loading the DTR through an ajax request
				$data['headscripts']['js'][1] = base_url()."v2includes/js/hr.dashboard.js";
			} else {
				if ($isgetfrom == "getfrom") {
					
					$area = $this->uri->segment(4);
					if ($area == null) {
						die("wrong way... go back");
					} else {
						$arcode     		= $this->uri->segment(4);
						$data['attendance'] = $this->Dashboard->getfrom($arcode);
						$data['issaved']	= $this->Dashboard->save_sync_data_log($data['attendance']['data'], $arcode);
						
					}
				}
			}
						
			$data['main_content'] = "v2views/hr_dashboard";
			$this->load->view('hrmis/admin_view',$data);
		}
		
		function dtr() {
			$this->load->model("v2main/Globalproc");
			$this->load->model('main/main_model');
			// called in ajax
			$data['title'] = '| Daily Time Records';

			if($this->session->userdata('is_logged_in')!=TRUE){
				  redirect('/accounts/login/', 'refresh');
			}else{
			
			$data['biometric_id'] 		= $this->session->userdata('biometric_id');
			$data['employee_id'] 		= $this->session->userdata('employee_id');
			$data['usertype'] 			= $this->session->userdata('usertype');
			$data['dbm_sub_pap_id'] 	= $this->session->userdata('dbm_sub_pap_id');
			$data['division_id'] 		= $this->session->userdata('division_id');
			$data['level_sub_pap_div'] 	= $this->session->userdata('level_sub_pap_div');
			$data['employment_type'] 	= $this->session->userdata('employment_type');
			$data['is_head'] 			= $this->session->userdata('is_head');
			
		//	$empdata 					  = $this->Globalproc->get_details_from_table("employees",
		//																			 ["employee_id"=>$data['employee_id']],
		//																			 ['e_signature']);
		//	$data['signature']["emp_sig"] = $empdata['e_signature'];
			
			$emp   = $this->input->post("emp_id");
			
			$data['retwat'] = $this->input->post("retwat");
			
			if ($emp != null || !empty($emp)) {
				
				$empid = $emp;
				
				$details = ['biometric_id',
							'DBM_Pap_id',
							'Division_id',
							'Level_sub_pap_div',
							'employment_type',
							'is_head',
							"e_signature",
							"area_id",
							"f_name"];
				$empdata = $this->main_model->array_utf8_encode_recursive( $this->Globalproc->get_details_from_table("employees",["employee_id"=>$empid],$details) );
				
				$data['biometric_id'] 		  = $empdata['biometric_id']; 
				$data['employee_id'] 		  = $empid; 
				$data['dbm_sub_pap_id'] 	  = $empdata['DBM_Pap_id']; 
				$data['division_id'] 		  = $empdata['Division_id']; 
				$data['level_sub_pap_div'] 	  = $empdata['Level_sub_pap_div']; 
				$data['employment_type'] 	  = $empdata['employment_type']; 
				$data['is_head'] 			  = $empdata['is_head']; 
				
				$utype = $this->Globalproc->get_details_from_table("users",["employee_id"=>$empid],['usertype']);
				
				$data['usertype'] 			  = $utype['usertype'];
				
				$data['get_margin'] = true;
				
				$data['signature']["emp_sig"] = $empdata['e_signature'];
				
				$token_details	   			  = $this->Globalproc->gdtf("countersign",
																		["emp_id"=>$empid,
																		 "conn"=>"and",
																		 "countersign_id"=>$this->input->post("cntid")],
																		['vercode',
																		 'dtr_summary_rep',
																		 'tobeapprovedby',
																		 'last_approving',
																		 'approval_status']);
			
				$data['token']['emp']	 	  = $token_details[0]->vercode."/".$token_details[0]->dtr_summary_rep."/".$empid;
				$data['token']['chief']		  = $token_details[0]->vercode."/".$token_details[0]->dtr_summary_rep."/".$token_details[0]->tobeapprovedby;
				$data['verificationcode']	  = $token_details[0]->vercode;
				
				// hide the print button beside the filter button 
				//	$data['print_hide'] = true;
				
				// cntid
				
				/*
				$chief = $this->Globalproc->get_details_from_table("employees",
																   ["Division_id"=>$empdata['Division_id'],"conn"=>"and", "is_head"=>1],
																   ['e_signature']);
				
				if ($token_details[0]) {
					$data['signature']['chief_sig']  = $chief['e_signature'];
				}
				*/
				
				$data['signature']['chief_sig'] = null;
				$data['signature']['last_sig']  = null;
				
				// user log in
					$data['uname'] = null;
					$data['upwd']  = null;
				// end 
				
				if($token_details[0]->approval_status == 1 || $token_details[0]->approval_status == 2) {
					$cs = $this->Globalproc->gdtf("employees",['employee_id'=>$token_details[0]->tobeapprovedby],["e_signature","email_2"]);
					$data['signature']['chief_sig'] = $cs[0]->e_signature;
				}
				
				if($token_details[0]->approval_status == 2) {
					$la = $this->Globalproc->gdtf("employees",['employee_id'=>$token_details[0]->last_approving],["e_signature","email_2","f_name"]);
					$data['signature']['last_sig']  = $la[0]->e_signature;
					$data['last_sig_name'] 			= $la[0]->f_name;
				}
				
				if ($token_details[0]->approval_status == 0) {
					$u_log 		   = $this->Globalproc->gdtf("users",["employee_id"=>$token_details[0]->tobeapprovedby],['Username','Password']);
					$data['uname'] = $u_log[0]->Username;
					$data['upwd']  = $u_log[0]->Password;
				} else if ($token_details[0]->approval_status == 1) {
					$u_log 		   = $this->Globalproc->gdtf("users",["employee_id"=>$token_details[0]->last_approving],['Username','Password']);
					$data['uname'] = $u_log[0]->Username;
					$data['upwd']  = $u_log[0]->Password;
				}
				
				//if (isset($this->input->post("coverage"))) {
					/*
					$coverage = null;
					if (isset($this->input->post("sumrep"))) {
						$sumrep = $this->input->post("sumrep");
						$coverage = $this->Globalproc->gdtf("dtr_summary_reports",
															["sum_reports_id"=>$sumrep],
															["dtr_coverage"])[0]->dtr_coverage;
					} else {
						$coverage = $this->input->post("coverage");
					}
					*/
					
					$coverage = $this->input->post("coverage");
					list($from,$to) = explode("-",$coverage);
					
					echo "<script>";
						echo "var getdate__  = true;";
						echo "var datefrom   = '{$from}';";
						echo "var dateto     = '{$to}';";
						echo "var bioid      = '{$data['biometric_id']}';";
						echo "var areaid     = '{$empdata['area_id']}';";
						echo "var empid      = '{$empid}';";
						echo "var empname    = '{$empdata['f_name']}';";
					echo "</script>";
					
					$from_ = date("m-d-Y",strtotime($from));
					$to_   = date("m-d-Y",strtotime($to));
					$data['accom_report'] = base_url()."/my/accomplishments/viewing/{$empid}/{$from_}/{$to_}";
				//}
			}
				
			$getemployees = $this->admin_model->getemployees();

			$getareas = $this->admin_model->getareas();

			$users = array();
		
			foreach ($getemployees as $rr) {
				$users[] = array('userid' => $rr->biometric_id , 'name' => $rr->f_name);
			}

			$data['areas'] = $getareas;


			$data['sub_pap_division_tree'] = $this->personnel_model->getsubpap_divisions_tree();
			$data['dtrformat'] = $this->admin_model->getdtrformat();
			$data['dbusers'] = $getemployees;
			
			$this->load->view("v2views/dtr_new_view", $data);

			}
		}
		
		function syncbio() {
			$this->load->model("v2main/Globalproc");
			
			$area_code 			   = $this->uri->segment(3);
			
			if ($area_code == null) {
				$data['area_null'] = "Area code is empty in the url. Please select an area.";
			} else {			
				$area   			   = $this->Globalproc->get_details_from_table("areas",["area_code"=>$area_code],["area_id","area_name"]);
				
				$data['name']		   = $area['area_name'];
				$data['area_code']	   = $area_code;
				
				$area_id 			   = $area['area_id'];
								
				$data['latest_update'] = $this->attendance_model->get_attemndance_logs_last_update($area_id); // mark 2
				$data['areas']		   = $this->admin_model->getareas();
			}
						
			$data['title'] 		   = '| Attendance Record';
			$data['main_content']  = 'hrmis/attendance/attendance_record_view';
			
			$this->load->view('hrmis/attendance/attendance_record_view');
		}
		
		function countersign() {
			$data['title'] 		   = '| Counter Sign';
			$data['main_content'] = "v2views/countersign";
			$this->load->view('hrmis/admin_view',$data);
		}

		
		function importdata() {
			if($this->session->userdata('is_logged_in')!=TRUE){
				  redirect('/accounts/login/', 'refresh');
			}else{

			$getareas = $this->admin_model->getareas();

			$data['title'] = '| Import Timelogs';
			$data['areas'] = $getareas;
			
			$this->load->view('hrmis/attendance/import_attendance_view', $data);
			// $this->load->view('hrmis/admin_view',$data);
			}
		}
		
		function shifts() {
			$data['shifts'] =  $this->attendance_model->getshifts();
			$data['title'] = '| Shift Management';
			$this->load->view('hrmis/attendance/shift_schedule_view',$data);
		}
		
		function schedules() {
			$getallemployeeshift = $this->attendance_model->getallemployeeshift();
	
			$data['employees'] = $getallemployeeshift;
			$data['title'] = '| Employee Schedule';
			$data['shifts'] =  $this->attendance_model->getshifts();

			$this->load->view('hrmis/attendance/timetable_view',$data);
		}
		
		function offices() {
			$this->load->model('personnel_model');

			$data['sub_pap_division_tree'] = $this->personnel_model->getsubpap_divisions_tree();

			$data['title'] = '| Division Setup';
			$this->load->view('hrmis/personnel/department_view',$data);
		}
		
		function areas() {
			$getareas = $this->admin_model->getareas();

			$data['title'] = '| Area Setup';
			$data['areas'] = $getareas;
			$this->load->view('hrmis/personnel/area_view',$data);
		}
		
		function positions() {
			$this->load->model('personnel_model');

			$data['title'] = '| Positions';
			$data['get_positions'] = $this->personnel_model->getpositions('NULL');
			$this->load->view('hrmis/personnel/position_view',$data);
		}
		
		function employees($url = "") {
			if($this->session->userdata('is_logged_in')!=TRUE){
				  redirect('/accounts/login/', 'refresh');
			}else{
				$main_content = null;
				$getemployees = $this->admin_model->getemployees();
				$getareas = $this->admin_model->getareas();

					$data['title'] = '| Employees';

				if($url == ''){
					$data['employees'] = $getemployees;
					$data['areas'] = $getareas;
					$data['sub_pap_division_tree'] = $this->personnel_model->getsubpap_divisions_tree();
					$data['positions'] = $this->personnel_model->getpositions('NULL');
					$main_content      = 'hrmis/personnel/employee_view';

				}else if ($url == 'import'){

					$data['areas'] = $getareas;

					$data['page_type'] = 'Edit';
					$main_content  = 'hrmis/personnel/import_employee_view';	

				}

			$this->load->view($main_content,$data);
			
			}
		}
		
		function summary() {
			if($this->session->userdata('is_logged_in')!=TRUE){
			    redirect('/accounts/login/', 'refresh');
			}else{
				$data['title'] = '| Summary Reports';
				$data['usertype'] = $this->session->userdata('usertype');

				$this->load->model('personnel_model');
				$this->load->view('hrmis/reports/summary_view',$data);
			}
		}
	}

?>