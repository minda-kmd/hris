<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	Class Accounts extends CI_Controller{
	

		public function __construct(){
			parent::__construct();
			$this->load->model('admin_model');

		}

		public function index(){
			$data['main_content'] = 'admin/dashboard';
			$this->load->view('login_view',$data);
		}

		public function login(){
			/*
			$this->load->model("Globalvars");
			
			$emp_id    = $this->Globalvars->employeeid;
			
			if ($emp_id != null) {
				redirect('http://office.minda.gov.ph:9003','refresh');
			}
			*/
	      	$data['main_content'] = 'login_view';
			$this->load->view('hrmis/login_view',$data);
      
		}

		public function logout(){

			$this->session->unset_userdata('is_logged_in');
	      	$this->session->unset_userdata('employee_id');
		    $this->session->unset_userdata('username');
		    $this->session->unset_userdata('usertype');
		    $this->session->unset_userdata('full_name');

		    $this->session->unset_userdata('area_id');
		    $this->session->unset_userdata('ip_address');
		    $this->session->unset_userdata('area_name');
		    $this->session->unset_userdata('database_default');

		    $this->session->sess_destroy();

			redirect('/accounts/login/', 'refresh');
		}



}