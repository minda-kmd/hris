<div class='content-wrapper' style='padding:0px;'> <!-- content wrapper -->
	<section class="content" style='padding:0px;'> <!-- section class content -->
      <div class='content_wrapper'>
		<div class='employ_cat'>
			<div class='btn-group'>
				<button class='btn btn-primary' id='approveall'> <i class="fa fa-thumbs-up"></i> &nbsp; Approve All </button>
			</div>
		</div>
		<h4 style='padding: 0px 11px;'> DTR: For Review </h4>
		<div class='review_cont'>
			<ul id='listofpeople'>
				<?php 
					if (count($forrev)==0) {
						echo "<p style='text-align: center;'> No DTR found </p>";
					} else {
						foreach($forrev as $f) {
							$approval_status = null;
							if ($f->approval_status == 1) {
								$approval_status = "<p style='background: #90d0ef; padding: 4px; float: left; font-size: 12px;'>Approved by Chief</p> <div style='clear:both;'></div>";
							} else if ($f->approval_status == 2) {
								$approval_status = "<p style='background: #40d8a7; padding: 4px; float: left; font-size: 12px;'>this DTR is approved</p> <div style='clear:both;'></div>";
							}
							echo "<li 
									class='btn btn-default'
									data-emp_id = '{$f->emp_id}'
									data-retwat = '2'
									data-cntid  = '{$f->cnt_id}'
									data-coverage = '{$f->date_start_cover} - {$f->date_end_cover}'>
										<p class='thefname'> {$f->f_name} </p>
										<p> Cover date: <strong> {$f->date_start_cover} - {$f->date_end_cover}</strong> </p>
										{$approval_status}
								  </li>";
						}
					} //{$f->Division_desc}
				?>
			</ul>
		</div>
	  </div>
	</section>
</div>

<div class="modal fade" id="dtrforcheck" tabindex="-1" role="dialog" aria-labelledby="label_exceptions" aria-hidden="true" style="display: none;">
    <div class="modal-lg modal-dialog rewidth">
        <div class="modal-content">
			<div class='modal-body bodyoverflow'>
				<div class='floatleft the_dtr_view' id='thedtr'>
					
				</div>
				<div class='floatleft leftdetails'>
					<div class='btnholder the_dtr_details'>
						<div class='makecenter'>
							<button class='findingbtns btn btn-default' id='addfinding'> Add a finding </button>
							<?php if(!isset($chief_here)): ?>
								<button class='findingbtns btn btn-primary' id='allowsubmit'> Cleared </button>
							<?php endif; ?>
							
							<?php if(isset($chief_here)): ?>
								<a href='' target='_blank' class='findingbtns btn btn-primary' id='approvedtr'> Approve </a>
							<?php endif; ?>
						</div>
					</div>
				
					<div class='findingdiv' id='findingdiv'>
						<p> <strong> Date in DTR </strong> </p>
						<p> <input type='text' class='form-control' id='findingdate'/> </p>
						<p> <strong> Your Finding </strong> </p>
						<p> <textarea class='form-control' id='themsgtext'></textarea> </p>
						<p> <button class='btn btn-primary' id='addnow'> Add </button> </p>
						
						<div class='thefindings'>
							<p> <strong> Findings </strong> </p> 
								<p class='nofindingsyet'> None </p>
								<ul id='findingsul'>
									
								</ul>
							<hr/>
							<p style='text-align:center;'>
							<button class='btn btn-primary' id='returnbackwithfindings' data-findingsfrom='<?php echo $findingsfrom; ?>'> Return to owner </button>
							</p>
						</div>
					</div>
					<p id='statustext'> </p>
				</div>
			</div>
		</div>
	</div>
</div>