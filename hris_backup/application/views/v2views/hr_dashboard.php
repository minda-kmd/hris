<div class="content-wrapper" style='margin-left: 230px !important;'>
	<section class="content" style='padding:0px;'>
	
<div class='hr_overall_wrap' >
	<div class='hr_wrapper'>
			<div class='hr_drop_navs'>
			<!-- hr main navigation hr_main_navs leavecabinet-->
				<div class='hr_main_navs leavecabinet'>
					<ul>
						<?php //if ($admin): ?>
						<li> <p data-link='http://office.minda.gov.ph:9003/leave/management/'> <i class="fa fa-tasks" aria-hidden="true"></i> Leave Management </p> </li>
						<li> <p data-link='http://office.minda.gov.ph:9003/hr/dtr'> <i class="fa fa-wpforms" aria-hidden="true"></i> DTR - HR view</p> </li>
						<li> <p data-link='http://office.minda.gov.ph:9003/hr/summary'> <i class="fa fa-list-alt" aria-hidden="true"></i> Summary Reports </p> </li>
						<li title='Import Log'> <p data-link='http://office.minda.gov.ph:9003/hr/importdata'> <i class="fa fa-upload" aria-hidden="true"></i> &nbsp; Import Log </p> </li>
						<li title='Shifts'> <p data-link='http://office.minda.gov.ph:9003/hr/shifts'> <i class="fa fa-sitemap" aria-hidden="true"></i>&nbsp; Shifts </p> </li>
						<li title='Schedules'> <p data-link='http://office.minda.gov.ph:9003/hr/schedules'> <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp; Schedules </p> </li>
						<li title='Offices'> <p data-link='http://office.minda.gov.ph:9003/hr/offices'> <i class="fa fa-building" aria-hidden="true"></i>&nbsp; Offices </p> </li>
						<li title='Areas'> <p data-link='http://office.minda.gov.ph:9003/hr/areas'> <i class="fa fa-map-o" aria-hidden="true"></i>&nbsp; Areas </p> </li>
						<li title='Positions'> <p data-link='http://office.minda.gov.ph:9003/hr/positions'> <i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp; Positions </p> </li>
						<li title='Employees'> <p data-link='http://office.minda.gov.ph:9003/hr/employees'> <i class="fa fa-users" aria-hidden="true"></i>&nbsp; Employees </p> </li>
						<li title='Sync Bio' id='sync_bios'> 
						<p> <i class="fa fa-refresh" aria-hidden="true"></i> &nbsp; Sync Biometrics</p> 
							<ul>
								<?php 
									$url = "http://".$_SERVER['HTTP_HOST']."/hr/dashboard";
									foreach($syncs as $lis) {
										echo "<li>
											<a href='{$url}/getfrom/{$lis['code']}'>";
										echo "<p><i class='fa fa-map-signs' aria-hidden='true'></i> &nbsp;";
											echo "<span>{$lis['name']}</span> ";
											// echo "<span>{$lis['code']}</span> | ";
											echo "<span>[{$lis['ip']}]</span> ";
											echo "<span style='font-size: 13px; color: #847474; float: right; margin-top: -9px;'>
													<strong style='font-size: 11px !important; float: right;'>Last Update:</strong> <br/> 
													<i class='fa fa-calendar' aria-hidden='true'></i> {$lis['last_update']}
												  </span>";
										echo "</p></a>
											</li>";
									}
								?>
							</ul>
						</li>
						<?php// endif; ?>
						<?php if(!$admin): ?>
							<!--li> <p data-link='http://office.minda.gov.ph:9003/hr/dtr'> <i class="fa fa-wpforms" aria-hidden="true"></i> My DTR </p> </li-->
						<?php endif; ?>
					</ul>
				</div> 
			<!-- end of hr main navigation -->
			</div>
			
		<!-- used in loading the DTR -->
		<span id='element_loader'></span>
		<!-- end loading DTR -->
		
		<?php 
			
			if (isset($issaved)) {
				if ($issaved != false) {
					echo "<p style='padding: 33px 0px 0px; text-align: center;'> Data in database has been successfully synchronized with the biometrics' at ".$issaved.".</p>";
					echo "<p style='text-align:center;'> WARNING: reloading this page means you are trying to reprocess the transaction again. </p>";
					echo "<p style='text-align:center;'> To view DTR click <a href='{$url}'>here</a> </p>";
				} else {
					echo "<p> There was an error. </p>";
				}
			}
		?>
		<div class='showwindow' id='showwindow'>
			<div id='pop_upwindow'>
				
			</div>
		</div>
	</div>
</div>

	</section>
</div>