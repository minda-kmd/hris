<div class='content-wrapper' style='padding:0px;'> <!-- content wrapper -->
	<section class="content" style='padding:0px;'> <!-- section class content -->
      <div class='content_wrapper'>
		<p class='otcto_text'> Table of Overtime and Compensatory Time-Off </p>
		<table class='cto_ot_tbl'>
			<tr> 
				<td colspan=15 id='thename'> <?php echo $emp_name; ?> </td>
			</tr>
			<tr>
				<td colspan=10 style='background: #e8e8e8; font-weight: bold;'>
					Overtime 
				</td>
				<td colspan=5 style='background: #ddfff4; font-weight: bold;'>
					Compensatory Time-Off
				</td>
			</tr>
			<tr>
				<th rowspan=2> Date of Overtime </th>
				<th rowspan=2> Day </th>
				<th rowspan=2 colspan=2> AM </th>
				<th rowspan=2 colspan=2> PM </th>
				<th rowspan=2> Total </th>
				<th colspan=3> Credits </th>
				<th rowspan=2> Date of CTO </th>
				<th colspan=2> Used COC's </th>
				<th rowspan=2> Remaining COC (in hours) </th>
				<th rowspan=2> Excess OT </th>
			</tr>
			<tr>
				<th> 1x </th>
				<th> 1.5x </th>
				<th> Total </th>
				<th> Hrs </th>
				<th> Mins </th>
			</tr>
			
			<tbody id='tbody_contents'>
				<!--tr>
						<td> Month x, xxxx </td>
						<td> Xxxxxxx </td>
						<td> 08:00 AM </td>
						<td> 12:00 PM </td>
						<td> 12:00 PM </td>
						<td> 05:00 PM </td>
						<td> 8.00 </td>
						<td>  </td>
						<td> here </td>
						<td> 12 </td>
					
						<td>  </td>
						<td>  </td>
						<td>  </td>
						<td> 24 </td>
						<td>  </td>
					
				</tr-->
			</tbody>
		</table>	
	  </div>
	</section>
</div>