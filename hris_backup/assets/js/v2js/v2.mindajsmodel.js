// written: Alvin merto
// requires jquery to perform the functions below
var minda = jQuery;

function performajax(val, somefunction) {
	minda.ajax({
		url 		: BASE_URL+val[0],
		type 		: "POST",
		data 		: { info: val[1] },
		dataType	: "json",
		success 	:function(data) {
			somefunction(data);
		},
		error 		: function(a,b,c) {
			console.log("error:")
			console.log(val);
		}
	})
	
}

function convertthis(date) {
	var months = ["January","February","March","April","May","June",
				  "July","August","September","August","September",
				  "October","November","December"];
	
	// raw format 
		// year-month-day
		// :: 2017-10-30
	
	var thedate = new Object();
		thedate.month = date.substring(5, date.length-3);
		thedate.year  = date.substring(0,4);
		thedate.day   = date.substring( date.length-2 , date.length);
	
		return months[thedate.month]+" "+thedate.day+", "+thedate.year;
}

function convert_this(ref, toconvert, type) {
	
	if (toconvert == 0) {
		return 0;
	}
	
	for(var i = 0; i <= ref.length-1; i++) {
		if (ref[i].particular == toconvert && ref[i].type == type) {
			return ref[i].equi_day;
		}
	}
}

function numberofworkingdays(from, to) {	
			var days = ["sunday","monday","tuesday","wednesday","thursday","friday",,"saturday"];
			var oneDay 		   = 24*60*60*1000;
			
			var current = new Object();
				current.month = null;
				current.day   = null;
				current.year  = null;
				
			var firstDate 	   = new Date( from.getFullYear() , from.getMonth() , from.getDate() );
			var secondDate 	   = new Date( to.getFullYear() , to.getMonth() , to.getDate() );
			var daysdiff 	   = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
				daysdiff 	   = daysdiff+1;
				// getDay
			
				// dates format day month num, year timezone
			var dates = [];
			
				for(var i = from.getDate() ; i <= parseInt(to.getDate())+1 ; i++) {
					
				}
				
			return daysdiff;
}

// start 


        function processdtr(sdate, edate , biometric_id , area_id , employee_id){


            $.ajax({
               type: 'POST',
               url: BASE_URL + 'reports/generatedtrs',
               data: { 'sdate' : sdate , 'edate' : edate , 'userid' : biometric_id , 'area_id' : area_id , 'employee_id' : employee_id },
               dataType : 'json',
               beforeSend: function(){
                
               },
               success: function (data) {
				
                 generatedtr(data.dtr_array);


                 var compute_lates = data.compute_lates;
                 $("#late_times").html(data.count_lates);
                 $("#late_d").html(compute_lates.day);
                 $("#late_h").html(compute_lates.hours);
                 $("#late_m").html(compute_lates.minutes);

                /* total lates display */
                 $('#total_lates_input_display').html(data.count_lates + 'x ' + compute_lates.day +':'+compute_lates.hours+':'+compute_lates.minutes);

                var compute_undertime = data.compute_undertime;
                 $("#un_times").html(data.count_undertime);
                 $("#un_d").html(compute_undertime.day);
                 $("#un_h").html(compute_undertime.hours);
                 $("#un_m").html(compute_undertime.minutes);


                $('#total_undertime_input_display').html(data.count_undertime + 'x ' + compute_undertime.day +':'+compute_undertime.hours+':'+compute_undertime.minutes);

                 var compute_absences = data.compute_absences;
                 $("#ab_times").html(data.count_absences);
                 $("#ab_d").html(compute_absences.day);
                 $("#ab_h").html(compute_absences.hours);
                 $("#ab_m").html(compute_absences.minutes);

                 $('#total_absences_input_display').html(data.count_absences + 'x ' + compute_absences.day +':'+compute_absences.hours+':'+compute_absences.minutes);

                 var compute_ps = data.compute_ps;
                 $('#ps_no_times').html(data.count_ps);
                 $('#ps_d_').html(compute_ps.day);
                 $('#ps_h_').html(compute_ps.hours);
                 $('#ps_m_').html(compute_ps.minutes);

                 $('#total_ps_input_display').html(data.count_ps + 'x ' + compute_ps.day +':'+compute_ps.hours+':'+compute_ps.minutes);               
                        

                 var compute_total = data.compute_total;
                 $("#to_times").html(data.count_total);
                 $("#to_d").html(compute_total.day);
                 $("#to_h").html(compute_total.hours);
                 $("#to_m").html(compute_total.minutes);

                 $('#total_input_display').html(data.count_total + 'x ' + compute_total.day +':'+compute_total.hours+':'+compute_total.minutes);   

                 $('#total_working_days').html(data.count_total_working_days);   
                 $('#total_services_r').html(data.count_total_services_rendered);   


                 $('#total_time_shift_sched').html(data.time_shift_schedule); 

                 if(data.temporary_time_shifts != ''){
                  $('#total_temporary_shift').html('TEMPORARY SHIFT/S: <br>'  + data.temporary_time_shifts);
                 }else{
                  $('#total_temporary_shift').html('');
                 }
                 
 
                 $('#total_tardiness_undertime').html(data.tardiness_undertime);   


                 $('#total_holidays').html(data.total_holidays);   
                 $('#total_holiday_services_r').html(data.total_holiday_services_r);   
                 $('#total_holiday_tardiness_undertime').html(data.total_holiday_tardiness_undertime);   


                 $('#label_official_hours').html(data.time_shift_schedule);

                 var employee_details = data.employee_details;

                 if(employee_details[0].employment_type == 'JO'){
                   $('.regular_table').hide();
                 }else{
                  $('.regular_table').show();
                 }


                 var shift = data.shift;
                 SHIFT_DATA = shift;
                 TEMPORARY_SHIFT = data.temporary_shift;


                 SUMMARY_REPORT_TARDINESS = data.summary_report_tardiness;
                 SUMMARY_REPORT_UNDERTIME = data.summary_report_undertime;
                 SUMMARY_REPORT_PS_PERSONAL = data.summary_report_ps_personal;
             
                var am_start = covertampmto24hour(shift[0].time_flexi_exact);
                var am_end  = covertampmto24hour(shift[1].time_flexi_exact);
                var pm_start = covertampmto24hour(shift[2].time_flexi_exact);
                var pm_end = covertampmto24hour(shift[3].time_flexi_exact);

          

                /* fill shifting */

                             $("#jqxtime_1").jqxDateTimeInput({ width: 'auto', height: '25px', formatString: 'hh:mm tt', showTimeButton: true, showCalendarButton: false});
                             var t = new Date(2016, 05, 16, am_start.hours , am_start.minutes, 0, 0);
                             $('#jqxtime_1').jqxDateTimeInput('setDate', t);


                             $("#jqxtime_2").jqxDateTimeInput({ width: 'auto', height: '25px', formatString: 'hh:mm tt', showTimeButton: true, showCalendarButton: false});
                             var t = new Date(2016, 05, 16, am_end.hours, am_end.minutes, 0, 0);     
                             $('#jqxtime_2').jqxDateTimeInput('setDate', t);  


                             $("#jqxtime_3").jqxDateTimeInput({ width: 'auto', height: '25px', formatString: 'hh:mm tt', showTimeButton: true, showCalendarButton: false});
                             var t = new Date(2016, 05, 16, pm_start.hours, pm_start.minutes, 0, 0);     
                             $('#jqxtime_3').jqxDateTimeInput('setDate', t);  


                             $("#jqxtime_4").jqxDateTimeInput({ width: 'auto', height: '25px', formatString: 'hh:mm tt', showTimeButton: true, showCalendarButton: false});
                             var t = new Date(2016, 05, 16, pm_end.hours, pm_end.minutes, 0, 0);     
                             $('#jqxtime_4').jqxDateTimeInput('setDate', t); 
 
        
  
                 $('#vl_times').html(data.count_vl);
                 $('#sl_times').html(data.count_sl);




                 $('#jqxemployee_id').val(employee_id);



                 if(data.shifting_msg){
                      console.log(data.shifting_msg);
                 }

               }

           });  
        }



    function generatedtr(dtr_array, user_id, user_name){

        var data = new Array();

        var today = new Date();
        var mm = today.getMonth()+1
        var today = mm + '/' + today.getDate() + '/'+today.getFullYear();



        var id = $('#jqxcombo').val(); // provide the id of the user
        var user_name =   $('#jqxfullname').val(); // provide the username of user 

        var index = 0;
        var index_1 = '';
       
          for (i in dtr_array){
        
              var row = {};
              var color = ''; 
              var display_span_holiday = '';

            if(dtr_array[i].IS_APPROVED == 1){
                color = "green";
            }else if (dtr_array[i].IS_APPROVED == 0){
                color = "red";
            }

            if(dtr_array[i].is_holiday == 1){
              display_span_holiday = ' <span style="font-weight:bold; font-size:11px; color:#9C27B0; ">(H)</span>';
            }


            if(dtr_array[i].EXCEPTION != '0' && dtr_array[i].EXCEPTION != ''){
               row["CHECKDATE"] = dtr_array[i].CHECKDATE + ' <span style="font-size:11px; color:'+color+'"> (' +dtr_array[i].EXCEPTION+ ') ' + display_span_holiday + ' </span>' ;
            }else{
               row["CHECKDATE"] = dtr_array[i].CHECKDATE + display_span_holiday;
            }




            var this_date_1 = dtr_array[i].CHECKDATE;
            var checkdate_1 = this_date_1.substring(0, this_date_1.length - 3);

              

            if(checkdate_1 == today){
                 index_1 = index;
                 row["CHECKDATE"] = '<span style="font-weight:bold; color: blue ; padding:1px;  border-radius:2px 2px;">' + row["CHECKDATE"] + display_span_holiday + '</span>';
            }


             index++;

             



           
            row["this_date"] = dtr_array[i].CHECKDATE;
            row["types_md"] = dtr_array[i].EXCEPTION;
            row["is_appro"] = dtr_array[i].IS_APPROVED;
            row["lates"] = dtr_array[i].lates;
            row["undertime"] = dtr_array[i].undertime;
            row["ps"] = dtr_array[i].final_ps;





            var f_ot =  dtr_array[i].final_ot;
            var final_ot = '';



            




            row["ot_id"] = dtr_array[i].ot_id;
            row["div_is_approved"] = dtr_array[i].div_is_approved;
            row["total_hours_rendered"] = dtr_array[i].total_hours_rendered;





            if(dtr_array[i].ot_id == 0 && dtr_array[i].div_is_approved == 0){

               final_ot = f_ot;

            }else{


              if(dtr_array[i].div_is_approved == 1){

                    if(f_ot != ''){
                      final_ot = '<span style="color:green;">' + f_ot + ' </span>';
                    }else{
                      final_ot = '<span style="color:green;">OT</span>';
                    }
               
              }else{

                    if(f_ot != ''){
                      final_ot = '<span style="color:red;">' + f_ot + ' </span>';
                    }else{
                      final_ot = '<span style="color:red;">OT</span>';
                    }

              }


            }



            row["ot"] = final_ot;


            var res_exact_id = dtr_array[i].exact_id;

            if(res_exact_id == ''){
                res_exact_id = 0;
            }

    
          
            row["exact_id"] = res_exact_id;
    
            row["TIME1"] = "";
            row["TIME2"] = "";
            row["TIME3"] = "";
            row["TIME4"] = "";
            row["TIME5"] = "";
            row["TIME6"] = "";

            var srow = dtr_array[i].CHECKINOUT;


            var c = 1;

            var cc = 0;
            var types_md = "0";

            for (s in srow){

                  var types_md = "";
                  var exact_id = "0";
                 
                  types_md = srow[s].TYPE_MODE;

                  var time_type_mode = '';

                    if(types_md == "0"){

                      time_type_mode = srow[s].TIME;
                    


                    }else if(types_md != '' && srow[s].TIME != ''){ 

                      var time = srow[s].TIME;

                      if(types_md == "LEAVE" || types_md == "PS" || types_md == "PAF" || types_md == "CTO" || types_md == "OT" ){ // || types_md == "CA"
                         time = '';
                      }

                       exact_id = srow[s].exact_id;
                       time_type_mode  = '<span id="'+srow[s].exact_id+'_'+srow[s].exact_log_id+'"style="color:'+color+';"> ' + time + ' </span>';

                    }


                    if (srow[s].TIME != ""){
                       cc++;
                    }



               row["TIME"+c] = time_type_mode;
               row['count'] = parseInt(cc) + parseInt(res_exact_id);

              c++;

            } 


                    data[i] = row;
          }

		//console.log(data);

            var source =
            {
                localdata: data,
                datatype: "array"
            };
            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }      
            });
            $("#jqxgrid").jqxGrid(
            {
                source: dataAdapter,
                autoheight:true,
                selectionmode: 'checkbox',
                //width:920,
                width:'100%',
                columns: [
                  { text: user_name, datafield: 'CHECKDATE'  ,width: 255},
                  { text: 'IN', datafield: 'TIME1' ,width: 120 ,  align: 'center' , cellsalign: 'center'},
                  { text: 'OUT', datafield: 'TIME2'  ,width: 120 ,  align: 'center' , cellsalign: 'center'},
                  { text: 'IN', datafield: 'TIME3' ,width: 120 ,  align: 'center' , cellsalign: 'center'},
                  { text: 'OUT', datafield: 'TIME4' ,width: 120 ,  align: 'center' , cellsalign: 'center'},
                  { text: 'IN', datafield: 'TIME5'   ,width: 120 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'OUT', datafield: 'TIME6' ,width: 120 ,   align: 'center' , cellsalign: 'center'},
                  { text: '<p style="margin:0px; font-size:9px; ">HOURS RENDERED</p><p style="font-size:9px; ">AM/PM</p>', datafield: 'total_hours_rendered' ,width: 130 ,   align: 'center' , cellsalign: 'center'},
                  { text: '<p style="margin:0px; font-size:9px; ">LATE</p><p style="font-size:9px; ">AM/PM</p>' ,width: 100 ,  datafield: 'lates' ,  align: 'center' , cellsalign: 'center'},
                  { text: '<p style="margin:0px; font-size:9px; ">UNDERTIME</p><p style="font-size:9px; ">AM/PM</p>' ,width: 100 , datafield: 'undertime' ,  align: 'center' , cellsalign: 'center'},
                  { text: '<p style="margin:0px; font-size:9px; ">PS</p><p style="font-size:9px; ">AM/PM</p>', datafield: 'ps' ,width: 100 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'OT', datafield: 'ot' ,width: 100 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'exact_id', datafield: 'exact_id' ,width: 50 ,   align: 'center' , cellsalign: 'center'},
                  //{ text: 'grp_id', datafield: 'grp_id' ,width: 50 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'count', datafield: 'count' ,width: 50 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'types_mode', datafield: 'types_md' ,width: 50 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'apprved', datafield: 'is_appro' ,width: 50 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'this_date', datafield: 'this_date' ,width: 100 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'ot_id', datafield: 'ot_id' ,width: 50 ,   align: 'center' , cellsalign: 'center'},
                  { text: 'ot_approved', datafield: 'div_is_approved' , width: 50 ,   align: 'center' , cellsalign: 'center'}                
                ]
            });   

    


            $("#jqxgrid").jqxGrid('hidecolumn', 'exact_id');
            $("#jqxgrid").jqxGrid('hidecolumn', 'count');
            $("#jqxgrid").jqxGrid('hidecolumn', 'types_md');
            $("#jqxgrid").jqxGrid('hidecolumn', 'is_appro');
            $("#jqxgrid").jqxGrid('hidecolumn', 'this_date');
            $("#jqxgrid").jqxGrid('hidecolumn', 'ot_id');
            $("#jqxgrid").jqxGrid('hidecolumn', 'div_is_approved');



            $('#jqxgrid').on('rowselect', function (event) {

                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
           
                var rowData = args.row;

                if (typeof rowBoundIndex === 'number') {
                    if (rowData.count === 4) {
                        $('#jqxgrid').jqxGrid('unselectrow', rowBoundIndex);
                    }
                } else if (typeof rowBoundIndex === 'object') {
                    var rows = $('#jqxgrid').jqxGrid('getrows');
                    for (var i = 0; i < rows.length; i++) {
                        if (rows[i].count === 4) {
                            $('#jqxgrid').jqxGrid('unselectrow', i);
                        }
                    }
                } 


                    var exact_id = rowData.exact_id;


                    if(exact_id != 0){

                        var rows = $('#jqxgrid').jqxGrid('getrows');
                        for (var i = 0; i < rows.length; i++) {
                            if (rows[i].exact_id == exact_id) {
                                $('#jqxgrid').jqxGrid('selectrow', i);
                            }else{
                                $('#jqxgrid').jqxGrid('unselectrow', i);
                            }
                        }
                   }



            });



            $('#jqxgrid').on('rowunselect', function (event) {

                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
    
                var rowData = args.row;

                  var exact_id = rowData.exact_id;


                    if(exact_id != 0){

                        var rows = $('#jqxgrid').jqxGrid('getrows');
                        for (var i = 0; i < rows.length; i++) {
                            if (rows[i].exact_id == exact_id) {
                                $('#jqxgrid').jqxGrid('unselectrow', i); 
                            }
                        }
                   }     


            });

/*
          if(index_1 != ''){
              $('#jqxgrid').jqxGrid('selectrow', index_1);
            }else{
               $('#jqxgrid').jqxGrid('clearselection');
            }*/

    }

// end

	function savetoactivity(empid, details, tablefrom, tableindex) {
		
		var ds 		       = new Object();
			ds.empid       = empid;
			ds.details     = JSON.stringify(details);
			ds.tablefrom   = tablefrom;
			ds.indexid     = tableindex;
			
		performajax(["App/s_act",{ dets: ds }], function(data) {
			console.log(data);
		});
		
	}
	
	function open_hris_upload_window() {
		minda("#upload_window").remove();
		
		minda("<div>",{ id: "upload_window" }).appendTo(document.body);
		minda("<div>",{ id: "upload_window_inner" }).appendTo("#upload_window");
		
			minda("<h4>",{ text:"hello world" , id : "upload_title" }).appendTo("#upload_window_inner");
	}



