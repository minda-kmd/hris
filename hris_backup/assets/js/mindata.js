var height;
var row_height;
updatesize(); //Handle change of height and width when resized


function updatesize(){

    // Get the dimensions of the viewport
    width = window.innerWidth || document.body.clientWidth;
    height = window.innerHeight || document.body.clientHeight;
    height = (height * 99)/100;
    row_height = (height * 8.5)/100; 

}



function notifyMe(info){

    var get_logs = postdata(BASE_URL + 'attendance/writeactivitylogs' ,info);
    if(get_logs){
           var info = {};                          
           info['logs'] = get_logs;
           var done = popupmes(info);
    }
}


function popupmes(info) {
    var NOTIFY_ENDPOINT = BASE_URL + "app/notify";

    var request = $.ajax({
        url: NOTIFY_ENDPOINT,
        data: {info: info}
    });
    request.done(function (msg) {
    });
}



function showmessage(message, type){

    $.notify({
      message: message
    },{
      type: type,
      z_index: 9999,
      placement: {
                from: "top",
                align: "right"
       },
      delay: 5000,
      timer: 1000,
      animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
      },
      offset: 20,
      onShow: null,
      onShown: null,
      onClose: null,
      onClosed: null,
      icon_type: 'class',
      template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="icon"></span> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
      '</div>' 
    });


}



function postdata(url,data){
  var result = jQuery.parseJSON($.ajax({
    url:  url,
    type:'post',
    dataType: "json", 
    data:{info:data},
    async: false
    }).responseText);  
    return result;
}


function binddata(url,fields){
    var source =
    {
        datatype: "json",
        datafields:fields,
        id: 'id',
        url: url,
        updaterow: function (rowid, newdata, commit) {
            
            commit(true);
        },
        addrow: function (rowid, rowdata, position, commit) {
            
            commit(true);
        },
        deleterow: function (rowid, commit) {
           
            commit(true);
        }
    };
  return source;
}


function binddata2(url,fields){
    var source =
    {
        datatype: "json",
        datafields:fields,
        id: 'id_orderid',
        url: url
    };
  return source;
}

function binddatanoasync(url,fields){
    var source =
    {
        datatype: "json",
        datafields:fields,
        id: 'id',
        url: url,
        async: false
    };
  return source;
}

function getandbinddata(url,data,fields){
    var source =
    {
        datatype: "json",
        datafields:fields,
        data:{info:data},
        type:'post',
        id: 'id',
        url: url
    };
  return source;
}
function getandbinddatanoasync(url,data,fields){
    var source =
    {
        datatype: "json",
        datafields:fields,
        data:{info:data},
        type:'post',
        id: 'id',
        url: url,
        async: false
    };
  return source;
}


function getfields(url,data){
    var result = jQuery.parseJSON($.ajax({
    url:  url,
    type:'post',
    dataType: "json", 
    data:{info:data},
    async: false
    }).responseText);  
    
   var fields = '[';
    $.each(result[0], function(key, element) {
        fields +='{ name: "'+key+'"},';
    });
    fields += '];';
    return eval(fields);
}


function uploadfile(url,data){
    
   var result = jQuery.parseJSON($.ajax({
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            async: false
       }).responseText);  
     return result;
}


function unserialize(data) {
  // From: http://phpjs.org/functions
  // +     original by: Arpad Ray (mailto:arpad@php.net)
  // +     improved by: Pedro Tainha (http://www.pedrotainha.com)
  // +     bugfixed by: dptr1988
  // +      revised by: d3x
  // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +        input by: Brett Zamir (http://brett-zamir.me)
  // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     improved by: Chris
  // +     improved by: James
  // +        input by: Martin (http://www.erlenwiese.de/)
  // +     bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     improved by: Le Torbi
  // +     input by: kilops
  // +     bugfixed by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Jaroslaw Czarniak
  // +     improved by: Eli Skeggs
  // %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
  // %            note: Aiming for PHP-compatibility, we have to translate objects to arrays
  // *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
  // *       returns 1: ['Kevin', 'van', 'Zonneveld']
  // *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
  // *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
  var that = this,
    utf8Overhead = function (chr) {
      // http://phpjs.org/functions/unserialize:571#comment_95906
      var code = chr.charCodeAt(0);
      if (code < 0x0080) {
        return 0;
      }
      if (code < 0x0800) {
        return 1;
      }
      return 2;
    },
    error = function (type, msg, filename, line) {
      throw new that.window[type](msg, filename, line);
    },
    read_until = function (data, offset, stopchr) {
      var i = 2, buf = [], chr = data.slice(offset, offset + 1);

      while (chr != stopchr) {
        if ((i + offset) > data.length) {
          error('Error', 'Invalid');
        }
        buf.push(chr);
        chr = data.slice(offset + (i - 1), offset + i);
        i += 1;
      }
      return [buf.length, buf.join('')];
    },
    read_chrs = function (data, offset, length) {
      var i, chr, buf;

      buf = [];
      for (i = 0; i < length; i++) {
        chr = data.slice(offset + (i - 1), offset + i);
        buf.push(chr);
        length -= utf8Overhead(chr);
      }
      return [buf.length, buf.join('')];
    },
    _unserialize = function (data, offset) {
      var dtype, dataoffset, keyandchrs, keys, contig,
        length, array, readdata, readData, ccount,
        stringlength, i, key, kprops, kchrs, vprops,
        vchrs, value, chrs = 0,
        typeconvert = function (x) {
          return x;
        };

      if (!offset) {
        offset = 0;
      }
      dtype = (data.slice(offset, offset + 1)).toLowerCase();

      dataoffset = offset + 2;

      switch (dtype) {
        case 'i':
          typeconvert = function (x) {
            return parseInt(x, 10);
          };
          readData = read_until(data, dataoffset, ';');
          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 1;
          break;
        case 'b':
          typeconvert = function (x) {
            return parseInt(x, 10) !== 0;
          };
          readData = read_until(data, dataoffset, ';');
          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 1;
          break;
        case 'd':
          typeconvert = function (x) {
            return parseFloat(x);
          };
          readData = read_until(data, dataoffset, ';');
          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 1;
          break;
        case 'n':
          readdata = null;
          break;
        case 's':
          ccount = read_until(data, dataoffset, ':');
          chrs = ccount[0];
          stringlength = ccount[1];
          dataoffset += chrs + 2;

          readData = read_chrs(data, dataoffset + 1, parseInt(stringlength, 10));
          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 2;
          if (chrs != parseInt(stringlength, 10) && chrs != readdata.length) {
            error('SyntaxError', 'String length mismatch');
          }
          break;
        case 'a':
          readdata = {};

          keyandchrs = read_until(data, dataoffset, ':');
          chrs = keyandchrs[0];
          keys = keyandchrs[1];
          dataoffset += chrs + 2;

          length = parseInt(keys, 10);
          contig = true;

          for (i = 0; i < length; i++) {
            kprops = _unserialize(data, dataoffset);
            kchrs = kprops[1];
            key = kprops[2];
            dataoffset += kchrs;

            vprops = _unserialize(data, dataoffset);
            vchrs = vprops[1];
            value = vprops[2];
            dataoffset += vchrs;

            if (key !== i)
              contig = false;

            readdata[key] = value;
          }

          if (contig) {
            array = new Array(length);
            for (i = 0; i < length; i++)
              array[i] = readdata[i];
            readdata = array;
          }

          dataoffset += 1;
          break;
        default:
          error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
          break;
      }
      return [dtype, dataoffset - offset, typeconvert(readdata)];
    }
  ;

  return _unserialize((data + ''), 0)[2];
}
function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

(function () {
  'use strict';

  function once(fn) {
    var called = false;

    if (typeof fn !== 'function') {
      throw new TypeError;
    }

    return function () {
      if (called) {
        throw new Error('Callback already called.');
      }
      called = true;
      fn.apply(this, arguments);
    }
  }

  function each(arr, next, cb) {
    var failed = false;
    var count = 0;

    cb = cb || function () {};

    if (!Array.isArray(arr)) {
      throw new TypeError('First argument must be an array');
    }

    if (typeof next !== 'function') {
      throw new TypeError('Second argument must be a function');
    }

    var len = arr.length;

    if (!len) {
      return cb();
    }

    function callback(err) {
      if (failed) {
        return;
      }

      if (err !== undefined && err !== null) {
        failed = true;
        return cb(err);
      }

      if (++count === len) {
        return cb();
      }
    }

    for (var i = 0; i < len; i++) {
      next(arr[i], i, once(callback));
    }
  }

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = each;
  } else {
    window.eachAsync = each;
  }
})();
