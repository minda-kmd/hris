function PusherNotifier(channel, options) {
    options = options || {};

    this.settings = {
        eventName: 'notification',
        title: 'Notification',
        titleEventProperty: 'title', // if set the 'title' will not be used and the title will be taken from the event
        eventTextProperty: 'message'
    };

    $.extend(this.settings, options);

    var self = this;
    channel.bind(this.settings.eventName, function(data){ self._handleNotification(data); });
};

PusherNotifier.prototype._handleNotification = function(data) {

   // console.log('test');

    //var title = data[this.settings.titleEventProperty];
    var title = SESSION_ID;
    //var data = data[this.settings.eventData];
    //var msg = data[this.settings.eventTextProperty];
    var msg = '';
    var logs = data.logs;
    //var createdbyname = data[this.settings.eventCreatedByName];
    // var owner_id = data[this.settings.eventOwnerId];
    //var owner_name = data[this.settings.evenetOwnerName];
    //var profile_pic = data[this.settings.profilePicture];
    //var profile_pic = 'http://192.168.1.9//assets/images/userImage.gif';

    for (i in logs){
        var assign_id = logs[i].assign_id;

        if(assign_id === title){

                generateCounterActivity();
                getactivities();

                msg = $( "#ul_activities li" ).first().html();
                div = '<div style=""> '+ msg +  '</div><div style="clear:both;"></div>';

                $.jGrowl(div,{
                    sticky: false,
                    theme: 'label-primary',
                position: 'bottom-left',
                animateOpen: { opacity: 'show' },
                animateClose: { opacity: 'hide' },
                life: '20000'
                });

        }
    }

};


  function generateCounterActivity(){
    var count_activity =  parseInt($('#label_count_activity').html());
    if(count_activity){
        count_activity+=1;
        $('#label_count_activity').html(count_activity);
    }else{
      $('#label_count_activity').html(1);
    }
  }

